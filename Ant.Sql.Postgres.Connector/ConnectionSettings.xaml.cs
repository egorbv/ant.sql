﻿using Ant.Sql.Common.WPF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Ant.Sql.Postgres.Connector
{
	/// <summary>
	/// Interaction logic for ConnectionSettings.xaml
	/// </summary>
	public partial class ConnectionSettings : DialogWindow
	{
		public ConnectionSettings()
		{
			InitializeComponent();
		}

		private void IconButton_Click(object sender, RoutedEventArgs e)
		{
			DialogResult = true;
		}
	}
}
