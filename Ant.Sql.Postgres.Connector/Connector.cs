﻿using Ant.Sql.Common.Management.Interfaces;
using Ant.Sql.Management.Postgres._10._5;
using Npgsql;
using System;
using System.Data.Common;
using System.Xml.Linq;

namespace Ant.Sql.Postgres.Connector
{
	public class Connector : IConnector
	{

		public XElement Create()
		{
			var dialog = new ConnectionSettings();
			if(dialog.ShowDialog() == true)
			{
				var connector = new XElement("Connector");
				connector.Add(new XAttribute("name", dialog.ServerName.Text));
				connector.Add(new XAttribute("host", dialog.ServerHost.Text));
				connector.Add(new XAttribute("user", dialog.ServerUser.Text));
				connector.Add(new XAttribute("password", dialog.ServerPassword.Text));
				return connector;
			}
			return null;
		}

		public XElement Edit(XElement settings)
		{
			var dialog = new ConnectionSettings();
			dialog.ServerName.Text = settings.Attribute("name").Value;
			dialog.ServerHost.Text = settings.Attribute("host").Value;
			dialog.ServerUser.Text = settings.Attribute("user").Value;
			dialog.ServerPassword.Text = settings.Attribute("password").Value;
			if (dialog.ShowDialog() == true)
			{
				var connector = new XElement("Connector");
				connector.Add(new XAttribute("name", dialog.ServerName.Text));
				connector.Add(new XAttribute("host", dialog.ServerHost.Text));
				connector.Add(new XAttribute("user", dialog.ServerUser.Text));
				connector.Add(new XAttribute("password", dialog.ServerPassword.Text));
				return connector; ;
			}
			return null;
		}

		public IServerManager GetServerManager(XElement settings)
		{
			var connectionManager = new ConnectorManager(settings);
			if(connectionManager.TryConnect())
			{
				var tmp = connectionManager.Version.Split('.');
				var tmpV = new int[4] { 0, 0, 0, 0 };
				for(var i=0; i < tmp.Length; i++)
				{
					tmpV[i] = int.Parse(tmp[i]);
				}
				//var version = new ObjectBrowserVersion(tmpV[0], tmpV[1], tmpV[2], tmpV[3]);
				//var objectBrowser = ObjectBrowserFactory.Get("Postgres", version);
				//objectBrowser.Initializate(connectionManager);
				var serverManager = new ServerManager();
				serverManager.Initializate(connectionManager);
				return serverManager;
			}
			return null;
		}

	}

	public class ConnectorManager : IConnectionManager
	{
		readonly string _connectionString;
		internal string Version;

		public ConnectorManager(XElement settings)
		{
			_connectionString = $"User ID={settings.Attribute("user").Value};Password={settings.Attribute("password").Value};Host={settings.Attribute("host").Value};Port=5432;Pooling=true;";
		}

		internal bool TryConnect()
		{
			try
			{
				using (var connection = new NpgsqlConnection(_connectionString))
				{
					connection.Open();
					var command = connection.CreateCommand();
					command.CommandText = "SHOW server_version";
					using (var reader = command.ExecuteReader())
					{
						if(!reader.Read())
						{
							throw new Exception("Не удалось получить версию сервера");
						}
						Version = reader.GetValue(0) as string;
					}
				}

			}
			catch(Exception e)
			{
				return false;
			}
			return true;
		}

		public DbConnection GetConnection()
		{
			var connection = new NpgsqlConnection(_connectionString);
			connection.Open();
			return connection;
		}

		public DbConnection GetConnection(string databaseName)
		{
			var connection = new NpgsqlConnection(_connectionString + $"Database={databaseName};");
			connection.Open();
			return connection;
		}
	}
}
