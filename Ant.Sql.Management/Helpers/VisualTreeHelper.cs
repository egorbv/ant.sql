﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace System.Windows
{
	public static class AntVisualTreeHelper
	{
		public static T GetVisualParent<T>(this DependencyObject element) where T : class
		{
			while (element != null)
			{
				if (element is T)
				{
					return element as T;
				}
				element = VisualTreeHelper.GetParent(element);
			}
			return null;
		}
	}
}
