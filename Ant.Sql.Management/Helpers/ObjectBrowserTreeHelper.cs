﻿using Ant.Sql.Common.Management.Interfaces;
using Ant.Sql.Common.Management.Models;
using Ant.Sql.Management.Controls;
using System.Collections.Generic;

namespace Ant.Sql.Management.Helpers
{
	public class ObjectBrowserTreeHelper
	{
		public static Models.TreeNodeContext GetContext(ObjectTreeViewItem treeNode)
		{
			var parents = new List<TreeNode>();
			var current = treeNode.Parent as ObjectTreeViewItem;
			IServerManager serverManager = null;
			if (treeNode.ServerManager != null)
			{
				serverManager = treeNode.ServerManager;
			}
			else
			{
				while (current != null)
				{
					if (current.ServerManager != null)
					{
						serverManager = current.ServerManager;
						break;
					}
					parents.Add(current.ObjectBrowserNode);
					current = current.Parent as ObjectTreeViewItem;
				}
			}
			return new Models.TreeNodeContext() { CurrentNode = treeNode.ObjectBrowserNode, Parents = parents, ServerManager = serverManager };
		}
	}
}
