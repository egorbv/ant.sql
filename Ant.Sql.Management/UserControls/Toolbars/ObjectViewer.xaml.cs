﻿using Ant.Sql.Common.Management;
using Ant.Sql.Common.Management.Models;
using Ant.Sql.Common.Wpf;
using Ant.Sql.Management.Triggers;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace Ant.Sql.Management.UserControls.Toolbars
{
	/// <summary>
	/// Interaction logic for ObjectViewer.xaml
	/// </summary>
	public partial class ObjectViewer : UserControl
	{
		public Command<ToolbarItem> ToolbarCommand { get; set; }

		public ObjectViewer()
		{
			InitializeComponent();
			ToolbarCommand = new Command<ToolbarItem>(item=>TriggerManager.Fire< ExecutedObjectViewerToolbarCommand>(item));
			TriggerManager.Subscribe<ChangedObjectViewerToolbarItems, List<ToolbarItem>>(_changedToolbarItems);
			Visibility = Visibility.Collapsed;
			DataContext = this;
		}

		void _changedToolbarItems(List<ToolbarItem> items)
		{
			OV.ItemsSource = items;
			Visibility = items.Count > 0 ? Visibility.Visible : Visibility.Collapsed;
		}
	}
}
