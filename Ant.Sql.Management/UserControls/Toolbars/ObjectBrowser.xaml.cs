﻿using Ant.Sql.Common.Management;
using Ant.Sql.Common.Management.Models;
using Ant.Sql.Common.Wpf;
using Ant.Sql.Management.Triggers;
using System.Windows.Controls;

namespace Ant.Sql.Management.UserControls.Toolbars
{
	/// <summary>
	/// Interaction logic for ObjectBrowser.xaml
	/// </summary>
	public partial class ObjectBrowser : UserControl
	{
		Models.TreeNodeContext _context;

		public Command<ToolbarItem> ToolbarCommand { get; set; }

		public ObjectBrowser()
		{
			InitializeComponent();
			TriggerManager.Subscribe<ChangedTreeObjectBrowserNodeTrigger, Models.TreeNodeContext>(_changedNode);
			ToolbarCommand = new Command<ToolbarItem>(item => _context.ServerManager.TreeToolbarService.Execute(_context, item.ActionID));
			DataContext = this;
			Visibility = System.Windows.Visibility.Collapsed;
		}

		void _changedNode(Models.TreeNodeContext context)
		{
			_context = context;
			if (context.ServerManager != null)
			{
				var items = context.ServerManager.TreeToolbarService.GetItems(context.CurrentNode);
				TreeToolBar.ItemsSource = items;
				Visibility = System.Windows.Visibility.Visible;
			}
			else
			{
				if(context.CurrentNode.Type == TreeNodeType.ConnectorOffline)
				{
					Visibility = System.Windows.Visibility.Visible;

				}
				else
				{
					Visibility = System.Windows.Visibility.Collapsed;
				}
			}
		}
	}
}
