﻿using Ant.Sql.Common.Management;
using Ant.Sql.Common.Management.Interfaces;
using Ant.Sql.Common.Management.Models;
using Ant.Sql.Common.Wpf;
using System.Windows;
using Ant.Sql.Management.Triggers;
using System.Windows.Controls;


namespace Ant.Sql.Management.UserControls.Toolbars
{
	/// <summary>
	/// Interaction logic for ServerManagerToolbar.xaml
	/// </summary>
	public partial class ServerManager : UserControl
	{
		IServerManager _serverManager;
		Models.TreeNodeContext _context;

		public Command<ToolbarItem> ToolbarCommand { get; set; }

		public ServerManager()
		{
			InitializeComponent();
			DataContext = this;
			ToolbarCommand = new Command<ToolbarItem>(n => _executeToolbarCommand(n));
			TriggerManager.Subscribe<ChangedTreeObjectBrowserNodeTrigger,  Models.TreeNodeContext>(_onChangedTreeObjectBrowserNode);
			Visibility = Visibility.Collapsed;
		}

		void _executeToolbarCommand(ToolbarItem item)
		{
			_serverManager.ExecuteToolbarAction(_context, item.ActionID);
		}

		void _onChangedTreeObjectBrowserNode(Models.TreeNodeContext context)
		{
			if(_serverManager != context.ServerManager)
			{
				_serverManager = context.ServerManager;
				SMTI.ItemsSource = null;
				if(_serverManager != null)
				{
					var items = _serverManager.GetToolbarItemsItems();
					SMTI.ItemsSource = items;
					if(items != null && items.Count > 0)
					{
						Visibility = Visibility.Visible;
					}
				}
			}
			_context = context;
		}
	}
}
