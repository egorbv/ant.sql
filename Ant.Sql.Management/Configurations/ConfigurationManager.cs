﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace Ant.Sql.Management.Configurations
{
	public class ConfigurationManager
	{
		static Dictionary<string, XDocument> _configurations;
		static object _sync;
		static string _rootFolder;

		XDocument _document;
		XElement _settings;
		XElement _objectBrowser;
		string _fileName;

		static ConfigurationManager()
		{
			_configurations = new Dictionary<string, XDocument>();
			_sync = new object();
		}


		ConfigurationManager(XDocument document, string fileName)
		{
			_document = document;
			_settings = document.Element("Settings");
			_objectBrowser = _settings.Element("ObjectBrowser");
			if(_settings == null)
			{
				throw new Exception("root node not  found");
			}
			_fileName = _rootFolder + "\\" + fileName + ".xml";
		}


		public static void Init(string rootFolder)
		{
			var userFolder = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
			_rootFolder +=  userFolder + "\\"+ rootFolder;
			if (!Directory.Exists(userFolder))
			{
				Directory.CreateDirectory(userFolder);
			}
		}

		public static ConfigurationManager Get(string fileName)
		{
			if (!_configurations.ContainsKey(fileName))
			{
				lock (_sync)
				{
					var fullFileName = _rootFolder + "\\" + fileName + ".xml";
					if(File.Exists(fullFileName))
					{
						using (var stream = File.Open(fullFileName, FileMode.Open))
						{
							var document = XDocument.Load(stream);
							_configurations.Add(fileName, document);
							return new ConfigurationManager(document, fullFileName);
						}

					}
					if (!_configurations.ContainsKey(fileName))
					{
						if(fileName == "settings")
						{
							_createFromResource("Ant.Sql.Management.Resources.settings.xml", fileName);
						}
					}
				}
			}
			return new ConfigurationManager(_configurations[fileName], fileName);
		}
		
		internal IEnumerable<string> ServerGroups
		{
			get
			{
				lock (_sync)
				{
					return from x in _settings.Element("ObjectBrowser").Elements("ServerGroup") select x.Attribute("name").Value;
				}
			}
		}

		internal XElement GetConnector(string serverGroupName, string connectorName)
		{
			return _objectBrowser.Elements("ServerGroup").
				Where(a => a.Attribute("name").Value == serverGroupName).First().
				Elements("Connector").Where(x => x.Attribute("name").Value == connectorName).FirstOrDefault();
		}

		internal List<string> GetConnectors(string serverGroupName)
		{
			var result = new List<string>();
			var serverGroup = _objectBrowser.Elements("ServerGroup").Where(a => a.Attribute("name").Value == serverGroupName).First();
			foreach (var item in serverGroup.Elements("Connector"))
			{
				result.Add(item.Attribute("name").Value);
			}
			return result;
		}

		internal void DeleteConnector(string serverGroupName, string connectorName)
		{
			_objectBrowser.Elements("ServerGroup").
				Where(a => a.Attribute("name").Value == serverGroupName).First().
				Elements("Connector").Where(x => x.Attribute("name").Value == connectorName).FirstOrDefault().Remove();
		}

		internal bool ExistsConnector(string serverGroupName, string connectorName)
		{
			lock (_sync)
			{
				var serverGroup = _settings.Element("ObjectBrowser").Elements("ServerGroup").Where(x=>x.Attribute("name").Value == serverGroupName).FirstOrDefault();
				if(serverGroup == null)
				{
					throw new Exception("Not found");
				}
				var s = serverGroup.Elements("Connector").Where(x => x.Attribute("name").Value == connectorName).FirstOrDefault();
				return s != null;
			}
		}

		internal void AddConnector(string serverGroupName, XElement connector)
		{
			var serverGroup = _objectBrowser.Elements("ServerGroup").Where(a => a.Attribute("name").Value == serverGroupName).First();
			if(connector.Attribute("name") == null)
			{
				throw new Exception("");
			}
			serverGroup.Add(connector);
		}

		internal void UpdateServerGroup(string oldName, string newName)
		{
			lock (_sync)
			{
				var serverGroup =_settings.Element("ObjectBrowser").Elements("ServerGroup").Where(x => x.Attribute("name").Value == oldName).FirstOrDefault();
				if(serverGroup != null)
				{
					serverGroup.Attribute("name").Value = newName;
				}
			}
		}

		internal void CreateServerGroup(string name)
		{
			lock (_sync)
			{
				_settings.Element("ObjectBrowser").Add(new XElement("ServerGroup", new XAttribute("name", name)));
			}
		}


		internal void DeleteServerGroup(string name)
		{
			lock(_sync)
			{
				_settings.Element("ObjectBrowser").Elements("ServerGroup").Where(x=> x.Attribute("name").Value == name).Remove();
			}
		}


		public void Save()
		{
			lock(_sync)
			{
				_document.Save(_fileName);
			}
		}


		static void _createFromResource(string resourceName, string fileName)
		{
			using (Stream s = typeof(MainWindow).Assembly.GetManifestResourceStream("Ant.Sql.Management.Resources.settings.xml"))
			{
				if (s == null)
				{
					throw new InvalidOperationException("Could not find embedded resource");
				}
				var document = XDocument.Load(s);
				_configurations.Add(fileName, document);
			}
		}
	}
}
