﻿using Ant.Sql.Common.Management.Interfaces;

namespace Ant.Sql.Management.Models
{
	/// <summary>
	/// Модель контекста узла дерева обозревателя объектов при
	/// совершении над какого либо действия
	/// </summary>
	public class TreeNodeContext : Common.Management.Models.TreeNodeContext
	{
		/// <summary>
		/// Текущий серверный менеджер
		/// </summary>
		internal IServerManager ServerManager;
	}
}
