﻿using Ant.Sql.Common.Management;
using Ant.Sql.Common.Management.Interfaces;
using Ant.Sql.Common.Management.Models;
using Ant.Sql.Common.Management.Triggers;
using Ant.Sql.Management.Triggers;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace Ant.Sql.Management.Controls
{
	public class ObjectViewer : TabControl, IObjectViewer
	{
		int _scriptNumber = 0;
		List<ToolbarItem> _toolbarItems;

		public ObjectViewer()
		{
			TriggerManager.Subscribe<ExecutedObjectViewerToolbarCommand, ToolbarItem>(_executeToolbarCommand);
			TriggerManager.Subscribe<ObjectViewerSqlEditor, ObjectViewerSqlEditorArgs>(_createSqlScriptEditor);
			TriggerManager.Subscribe<ObjectViewerElement, ObjectViewerElementArgs>(_createElement);
			KeyDown += ObjectViewer_KeyDown;
		}

		private void ObjectViewer_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
		{
			//if(Keyboard.IsKeyDown(Key.LeftCtrl) && e.Key == Key.F5)
			//{
			//	var s = 5;
			//}
			if (_toolbarItems != null && _toolbarItems.Count > 0)
			{
				var count = _toolbarItems.Count;
				for (var i = 0; i < count; i++)
				{
					if((int)e.Key == _toolbarItems[i].Key)
					{
						_executeToolbarCommand(_toolbarItems[i]);
						break;
					}
				}
			}
		}

		void _executeToolbarCommand(ToolbarItem item)
		{
			if(SelectedValue is TabItem tabItem)
			{
				var element = tabItem.Tag as IObjectViewerElement;
				if (tabItem.Content is SqlQueryEditor)
				{
					element.Execute(item.ActionID, (tabItem.Content as SqlQueryEditor).SqlEditor.Text);
				}
				else
				{
					element.Execute(item.ActionID, null);
				}
			}
		}

		void _createElement(ObjectViewerElementArgs args)
		{
			var tab = new TabItem();
			tab.Header = args.Header;
			tab.Content = args.Element;
			tab.Tag = args.Element;
			Items.Add(tab);
			tab.IsSelected = true;
			tab.Focus();
		}

		protected override void OnSelectionChanged(SelectionChangedEventArgs e)
		{
			base.OnSelectionChanged(e);
			if (e.AddedItems != null && e.AddedItems.Count == 1)
			{
				var element = (e.AddedItems[0] as TabItem).Tag as IObjectViewerElement;
				_toolbarItems = element.GetToolbarItems();
				TriggerManager.Fire<ChangedObjectViewerToolbarItems>(_toolbarItems);
			}
		}

		void _createSqlScriptEditor(ObjectViewerSqlEditorArgs args)
		{
			var tab = new TabItem();
			var sqlEditor = new SqlQueryEditor();
			sqlEditor.SqlEditor.Text = args.Script;
			sqlEditor.Properties.Children.Add(args.Element as UIElement);
			tab.Header = "SQLQuery -" + _scriptNumber;
			tab.Content = sqlEditor;
			tab.Tag = args.Element;
			Items.Add(tab);
			tab.IsSelected = true;
			tab.Focus();
			sqlEditor.Focus();
			_scriptNumber++;
		}
	}
}
