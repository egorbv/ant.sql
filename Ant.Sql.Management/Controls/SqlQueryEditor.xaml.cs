﻿using ICSharpCode.AvalonEdit.Highlighting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;

namespace Ant.Sql.Management.Controls
{
	/// <summary>
	/// Interaction logic for SqlQueryEditor.xaml
	/// </summary>
	public partial class SqlQueryEditor : UserControl
	{
		public SqlQueryEditor()
		{
			InitializeComponent();
			IHighlightingDefinition customHighlighting;
			using (Stream s = typeof(MainWindow).Assembly.GetManifestResourceStream("Ant.Sql.Management.Resources.postgres.10.5.xshd"))
			{
				if (s == null)
				{
					throw new InvalidOperationException("Could not find embedded resource");
				}
				using (XmlReader reader = new XmlTextReader(s))
				{
					customHighlighting = ICSharpCode.AvalonEdit.Highlighting.Xshd.HighlightingLoader.Load(reader, HighlightingManager.Instance);
				}
			}
			HighlightingManager.Instance.RegisterHighlighting("Postgres.10.5", new string[] { ".sql" }, customHighlighting);
			SqlEditor.SyntaxHighlighting = HighlightingManager.Instance.GetDefinition("Postgres.10.5");
			SqlEditor.Focus();
		}
		
	}
}
