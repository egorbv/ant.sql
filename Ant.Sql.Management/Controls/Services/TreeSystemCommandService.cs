﻿using Ant.Sql.Common.Management;
using Ant.Sql.Common.Management.Models;
using Ant.Sql.Common.Management.Triggers;
using System;
using System.Collections.Generic;
using System.Windows.Controls;

namespace Ant.Sql.Management.Controls.Services
{
	public class TreeSystemCommandService
	{
		TreeView _tree;
		Dictionary<TreeCommandType, Action<TreeSystemCommandArgs>> _systemActions;

		public TreeSystemCommandService(TreeView tree)
		{
			_tree = tree;
			_systemActions = new Dictionary<TreeCommandType, Action<TreeSystemCommandArgs>>();
			TriggerManager.Subscribe<TreeSystemCommand, TreeSystemCommandArgs>(_handler);
			_systemActions.Add(TreeCommandType.Refresh, _refresh);
			_systemActions.Add(TreeCommandType.Disconnect, _disconnect);
		}


		void _handler(TreeSystemCommandArgs args)
		{
			if (_systemActions.ContainsKey(args.CommandType))
			{
				_systemActions[args.CommandType](args);
			}
		}

		#region _refresh
		/// <summary>
		/// Обработчик обновления текущего узла
		/// </summary>
		/// <param name="args">Аргументы действия</param>
		void _refresh(TreeSystemCommandArgs args)
		{
			var node = _tree.SelectedValue as ObjectTreeViewItem;
			node.Items.Clear();
			node.WasExpand = false;
			if (node.IsExpanded)
			{
				node.IsExpanded = false;
				node.IsExpanded = true;
			}
		}
		#endregion

		#region _disconnect
		/// <summary>
		/// Обработчик отсоеденения от сервера
		/// </summary>
		/// <param name="args">Аргументы действия</param>
		void _disconnect(TreeSystemCommandArgs args)
		{
			var node = _tree.SelectedValue as ObjectTreeViewItem;
			while (node != null)
			{
				if (node.ObjectBrowserNode.Type == TreeNodeType.ConnectorOnline)
				{
					node.ObjectBrowserNode = new TreeNode(TreeNodeType.ConnectorOffline, null);
					node.Items.Clear();
					node.WasExpand = false;
					break;
				}
				node = node.Parent as ObjectTreeViewItem;
			}
		}
		#endregion
	}
}
