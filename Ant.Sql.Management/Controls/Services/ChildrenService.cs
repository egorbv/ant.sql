﻿using Ant.Sql.Common.Management.Interfaces;
using Ant.Sql.Common.Management.Models;
using Ant.Sql.Management.Configurations;
using Ant.Sql.Management.Helpers;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Ant.Sql.Management.Controls.Services
{
	public class ChildrenService
	{
		readonly ConfigurationManager _settings;
		readonly ObjectBrowserTree _tree;

		public ChildrenService(ObjectBrowserTree tree)
		{
			_settings = ConfigurationManager.Get("settings");
			_tree = tree;
			EventManager.RegisterClassHandler(typeof(ObjectBrowserTree), TreeViewItem.ExpandedEvent, new RoutedEventHandler(_onExpandNode));
			_createSubTree(_getRoot(), null);
		}


		void _onExpandNode(object sender, RoutedEventArgs e)
		{
			var treeNode = e.OriginalSource as ObjectTreeViewItem;
			if (!treeNode.WasExpand)
			{
				treeNode.WasExpand = true;
				treeNode.Items.Clear();
				switch (treeNode.ObjectBrowserNode.Type)
				{
					case TreeNodeType.ServerGroup:
						_createSubTree(_getConnectors(treeNode), treeNode);
						break;
					case TreeNodeType.ConnectorOffline:
						_connect(treeNode);
						break;
					default:
						_getServerManagerChildren(treeNode);
						break;
				}
			}
		}

		void _createSubTree(List<TreeNode> children, ObjectTreeViewItem treeNode)
		{
			var items = treeNode != null ? treeNode.Items : _tree.Items;
			if (children != null)
			{
				foreach (var child in children.OrderBy((x) => x.Name))
				{
					var item = new ObjectTreeViewItem() { Header = child.Name, ObjectBrowserNode = child };
					var subItem = new ObjectTreeViewItem();
					item.Items.Add(subItem);
					items.Add(item);
				}
			}

		}

		async void _getServerManagerChildren(ObjectTreeViewItem treeNode)
		{
			treeNode.Header = treeNode.ObjectBrowserNode.Name + " загружаю";
			var context = ObjectBrowserTreeHelper.GetContext(treeNode);
			List<TreeNode> children = null;
			await Task.Run(() =>
			{
				children = context.ServerManager.TreeChildrenService.Get(context);
			});
			treeNode.Header = treeNode.ObjectBrowserNode.Name;
			_createSubTree(children, treeNode);
		}


		#region system nodes
		List<TreeNode> _getRoot()
		{
			var list = new List<TreeNode>();
			foreach (var serverGroup in _settings.ServerGroups)
			{
				list.Add(new TreeNode(TreeNodeType.ServerGroup, serverGroup));
			}
			return list;
		}

		List<TreeNode> _getConnectors(ObjectTreeViewItem treeNode)
		{
			var nodes = new List<TreeNode>();
			var connectors = _settings.GetConnectors(treeNode.ObjectBrowserNode.Name);
			foreach (var connector in connectors)
			{
				nodes.Add(new TreeNode(TreeNodeType.ConnectorOffline, connector));
			}
			return nodes;

		}

		async void _connect(ObjectTreeViewItem treeNode)
		{
			var node = treeNode.ObjectBrowserNode;
			var connectorName = node.Name;
			var serverName = (treeNode.Parent as ObjectTreeViewItem).ObjectBrowserNode.Name;
			var result = new List<TreeNode>();
			var connector = new Ant.Sql.Postgres.Connector.Connector();

			List<TreeNode> children = null;
			treeNode.Header = treeNode.Header + " соеденяюсь";
			await Task.Run(() => {
				var connectorData = _settings.GetConnector(serverName, connectorName);
				var serverManager = connector.GetServerManager(connectorData);
				if (serverManager != null)
				{
					var context = new TreeNodeContext() { CurrentNode = node };
					treeNode.ServerManager = serverManager;
					children = serverManager.TreeChildrenService.Get(context);
				}
			});
			treeNode.ObjectBrowserNode = new TreeNode(TreeNodeType.ConnectorOnline, treeNode.ObjectBrowserNode.Name);
			if (children != null)
			{
				_createSubTree(children, treeNode);
				treeNode.Header = treeNode.ObjectBrowserNode.Name;
			}
			else
			{
				treeNode.WasExpand = false;
			}
		}
		#endregion
	}
}
