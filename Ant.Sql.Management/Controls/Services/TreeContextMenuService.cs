﻿using Ant.Sql.Common.Management.Interfaces;
using Ant.Sql.Common.Management.Models;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace Ant.Sql.Management.Controls.Services
{
	public class TreeContextMenuService
	{
		ObjectBrowserTree _tree;

		public TreeContextMenuService(ObjectBrowserTree tree)
		{
			_tree = tree;
			_tree.ContextMenuOpening += _tree_ContextMenuOpening;
			_tree.ContextMenu = new ContextMenu();
		}

		IServerManager _currentServerManager;
		private void _tree_ContextMenuOpening(object sender, ContextMenuEventArgs e)
		{
			e.Handled = true;
			var treeNode = (e.OriginalSource as DependencyObject).GetVisualParent<ObjectTreeViewItem>();
			if (treeNode != null)
			{
				treeNode.IsSelected = true;
				var currentNode = treeNode;
				while(currentNode != null)
				{
					if(currentNode.ServerManager != null)
					{
						_currentServerManager = currentNode.ServerManager;
						var context = new TreeNodeContext() { CurrentNode = treeNode.ObjectBrowserNode, Parents = new List<TreeNode>() };
						currentNode = treeNode.Parent as ObjectTreeViewItem;
						//переделать
						while(currentNode != null && currentNode.ObjectBrowserNode.Type != TreeNodeType.ConnectorOffline)
						{
							context.Parents.Add(currentNode.ObjectBrowserNode);
							currentNode = currentNode.Parent as ObjectTreeViewItem;
						}
						var menu = _currentServerManager.TreeChildrenService.GetContextMenu(context);
						if (menu != null)
						{
							void build (ItemCollection root, List<TreeContextMenuItem> items)
							{
								foreach (var item in items)
								{
									if (item == null)
									{
										root.Add(new Separator());
										continue;
									}
									var contextMenuItem = new MenuItem() { Header = item.Text, Tag = item };
									root.Add(contextMenuItem);
									if (item.Items != null)
									{
										build(contextMenuItem.Items, item.Items);
									}
									else
									{
										contextMenuItem.Click += ContextMenuItem_Click; ;

									}
								}
							};
							_tree.ContextMenu.Items.Clear();
							_tree.ContextMenu.Tag = context;
							build(_tree.ContextMenu.Items, menu.Items);
							e.Handled = false;
						}
						break;
					}
					currentNode = currentNode.Parent as ObjectTreeViewItem;
				}
			}

		}

		private void ContextMenuItem_Click(object sender, RoutedEventArgs e)
		{
			var context = _tree.ContextMenu.Tag as TreeNodeContext;
			var action = (e.OriginalSource as MenuItem).Tag as TreeContextMenuItem;
			_currentServerManager.TreeChildrenService.ExecuteTreeAction(context, action.ActionID);
		}
	}
}
