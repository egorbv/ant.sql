﻿using Ant.Sql.Common.Management.Interfaces;
using Ant.Sql.Common.Management.Models;
using System.Windows;
using System.Windows.Controls;

namespace Ant.Sql.Management.Controls
{
	public class ObjectTreeViewItem : TreeViewItem
	{
		public static readonly DependencyProperty ObjectBrowserNodeProperty;

		internal IServerManager ServerManager;

		internal bool WasExpand;

		internal TreeNode ObjectBrowserNode
		{
			get
			{
				return GetValue(ObjectBrowserNodeProperty) as TreeNode;
			}
			set
			{
				SetValue(ObjectBrowserNodeProperty, value);
			}
		}

		static ObjectTreeViewItem()
		{
			ObjectBrowserNodeProperty = DependencyProperty.Register("ObjectBrowserNode", typeof(TreeNode), typeof(ObjectTreeViewItem),
				new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.AffectsMeasure | FrameworkPropertyMetadataOptions.AffectsRender, null, null));

		}

	}
}
