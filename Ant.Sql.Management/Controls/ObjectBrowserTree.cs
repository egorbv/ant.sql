﻿using System.Windows;
using System.Windows.Controls;
using Ant.Sql.Management.Controls.Services;
using Ant.Sql.Common.Management.Interfaces;
using Ant.Sql.Common.Management.Models;
using Ant.Sql.Common.Management;
using Ant.Sql.Management.Helpers;
using Ant.Sql.Management.Triggers;

namespace Ant.Sql.Management.Controls
{

	public class ObjectBrowserTree : TreeView
	{
		readonly ChildrenService _childrenService;
		readonly TreeContextMenuService _treeContextMenuService;
		readonly TreeSystemCommandService _treeSystemCommandService;


		public ObjectBrowserTree()
		{
			//_treeToolbarService = new TreeToolbarService(this);
			_treeContextMenuService = new TreeContextMenuService(this);
			_treeSystemCommandService = new TreeSystemCommandService(this);
			_childrenService = new ChildrenService(this);
			this.SelectedItemChanged += ObjectBrowserTree_SelectedItemChanged;
		}

		private void ObjectBrowserTree_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
		{
			var treeNode = e.NewValue as ObjectTreeViewItem;
			var context = ObjectBrowserTreeHelper.GetContext(treeNode);
			TriggerManager.Fire<ChangedTreeObjectBrowserNodeTrigger>(context);
		}

		///public event ChangedServerManager OnChangedServerManager;

		public void ExecuteToolbarCommand(ToolbarItem item)
		{
			//_childrenService.
		}

		//private void _contextMenuItemClick(object sender, RoutedEventArgs e)
		//{
		//	var actionContext = ContextMenu.Tag as MenuActionContext;
		//	var contextMenuItem = (e.OriginalSource as System.Windows.Controls.MenuItem).Tag as Common.ObjectBrowser.ContextMenu.MenuItem;
		//	actionContext.ActionID = contextMenuItem.ActionID;
		//	var treeNode = actionContext.CurrentTreeNode as ObjectTreeViewItem;
		//	actionContext.ObjectBrowser.ExecuteContextMenuAction(actionContext);
			

		//	switch (actionContext.ReturnActionType)
		//	{
		//		case ReturnActionType.AddNode:
		//			_addNode(actionContext);
		//			break;
		//		case ReturnActionType.UpdateNode:
		//			treeNode.Header = actionContext.Parents[0].Name;
		//			break;
		//		case ReturnActionType.Drop:
		//			if (treeNode.Parent is ObjectBrowserTree)
		//			{
		//				Items.Remove(treeNode);
		//			}
		//			else
		//			{
		//				(treeNode.Parent as TreeViewItem).Items.Remove(treeNode);
		//			}
		//			break;
		//		case ReturnActionType.AddObjectView:
		//			//ObjectViewer.AddObject(actionContext.ObjectViewerText, actionContext.ObjectViewerControl as IObjectViewerContent);
		//			break;
		//	}
		//}

		//void _addNode(IActionContext context)
		//{
		//	var items = context.CurrentTreeNode == null ? Items : (context.CurrentTreeNode as ObjectTreeViewItem).Items;
		//	var treeNode = new ObjectTreeViewItem() { Header = context.AddingNode.Name, ObjectBrowserNode = context.AddingNode};
		//	var subTreeNode = new ObjectTreeViewItem();
		//	treeNode.Items.Add(subTreeNode);
		//	items.Add(treeNode);
		//}


		void _refreshNode(object sender, RoutedEventArgs e)
		{
			//var treeNode = ContextMenu.Tag as TreeViewItem;
			//treeNode.Items.Clear();
			//var proxyObjectBrowserNode = treeNode.Tag as ProxyObjectBrowserNopde;
			//proxyObjectBrowserNode.WasExpand = false;
			//_createSubTreeNodes(treeNode);
		}









	}


}
