﻿using Ant.Sql.Management.Configurations;
using System.Windows;

namespace Ant.Sql.Management
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
		//public ManagementFactory ManagementFactory { get; set; }

		protected override void OnStartup(StartupEventArgs e)
		{
			base.OnStartup(e);
			ConfigurationManager.Init("Ant.Sql.Managent");
			//ManagementFactory = new ManagementFactory();
			//ManagementFactory.Set<IObjectViewer>(new ObjectViewer());
			//ManagementFactory.Set<IObjectBrowser>(new ObjectBrowserTree());


			//ObjectBrowserFactory.Set("Postgres", typeof(Ant.Sql.Management.Postgres._10._5.ObjectBrowser.ObjectBrowser));
		}

		protected override void OnExit(ExitEventArgs e)
		{
			base.OnExit(e);
			var configuration = ConfigurationManager.Get("settings");
			configuration.Save();
		}
	}
}
