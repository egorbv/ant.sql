﻿using Ant.Sql.Common.WPF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Ant.Sql.Management.Dialogs
{
	/// <summary>
	/// Interaction logic for ServerGroupWindow.xaml
	/// </summary>
	public partial class ServerGroupWindow : DialogWindow
	{
		public ServerGroupWindow(string name = null)
		{
			InitializeComponent();
			if(name != null)
			{
				ServerGroupText.Text = name;
			}
		}

		private void IconButton_Click(object sender, RoutedEventArgs e)
		{
			DialogResult = true;
		}
	}
}
