﻿using Ant.Sql.Common.Management.Models;

namespace Ant.Sql.Management.Triggers
{
	public delegate void ExecutedObjectViewerToolbarCommand(ToolbarItem item);
}
