﻿using Ant.Sql.Common.Management.Models;

namespace Ant.Sql.Management.Triggers
{
	/// <summary>
	/// Триггер срабатывает при смене выделенного узла в дереве объектов сервера
	/// </summary>
	/// <param name="context">Текущий контекст выделенного узла</param>
	public delegate void ChangedTreeObjectBrowserNodeTrigger(TreeNodeContext context);
}
