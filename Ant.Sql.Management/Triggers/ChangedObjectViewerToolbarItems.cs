﻿using Ant.Sql.Common.Management.Models;
using System.Collections.Generic;

namespace Ant.Sql.Management.Triggers
{
	public delegate void ChangedObjectViewerToolbarItems(List<ToolbarItem> items);
}
