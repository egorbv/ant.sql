﻿using Ant.Sql.Common;
using Ant.Sql.Common.Management;
using Ant.Sql.Common.Management.Interfaces;
using Ant.Sql.Common.Management.Models;
using Ant.Sql.Common.Management.Triggers;
using Ant.Sql.Common.Wpf;
using Ant.Sql.Management.Controls;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;


namespace Ant.Sql.Management.ViewModals
{
	public class MainModel : INotifyPropertyChanged
	{

		public MainModel()
		{
		}
		public event PropertyChangedEventHandler PropertyChanged;

		public void OnPropertyChanged([CallerMemberName]string prop = "")
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
		}
	}
}
