﻿using System.Windows;

namespace Ant.Sql.Common.WPF
{
	/// <summary>
	/// Interaction logic for DropObject.xaml
	/// </summary>
	public partial class DropObject : Window
	{
		public DropObject(string question)
		{
			InitializeComponent();
			Title = "Удаление объекта";
			Question.Text = question;
		}
	}
}
