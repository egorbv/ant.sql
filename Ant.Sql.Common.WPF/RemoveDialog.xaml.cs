﻿using System.Windows;

namespace Ant.Sql.Common.WPF
{
	public partial class RemoveDialog : DialogWindow
	{
		public RemoveDialog(string title, string question) : base()
		{
			InitializeComponent();
			Title = title;
			QuestionTextBox.Text = question;
		}

		private void _okButtonClick(object sender, RoutedEventArgs e)
		{
			DialogResult = true;
		}
	}
}
