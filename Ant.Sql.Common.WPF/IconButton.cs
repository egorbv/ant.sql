﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Ant.Sql.Common.WPF
{
	public class IconButton : Button
	{
		public static readonly DependencyProperty IconWidthProperty;
		public static readonly DependencyProperty IconHeightProperty;
		public static readonly DependencyProperty IconProperty;
		public static readonly DependencyProperty HoverBackgroundProperty;
		public static readonly DependencyProperty HoverBorderBrushProperty;

		public Brush HoverBorderBrush
		{
			get { return GetValue(HoverBorderBrushProperty) as Brush; }
			set { SetValue(HoverBorderBrushProperty, value); }
		}

		public Brush HoverBackground
		{
			get { return GetValue(HoverBackgroundProperty) as Brush; }
			set { SetValue(HoverBackgroundProperty, value); }
		}


		public ControlTemplate Icon
		{
			get
			{
				return GetValue(IconProperty) as ControlTemplate;
			}
			set
			{
				SetValue(IconProperty, value);
			}
		}

		public double IconWidth
		{
			get { return (double)GetValue(IconWidthProperty);}
			set { SetValue(IconWidthProperty, value); }
		}

		public double IconHeight
		{
			get { return (double)GetValue(IconHeightProperty); }
			set { SetValue(IconHeightProperty, value); }
		}


		static IconButton()
		{


			HoverBorderBrushProperty = DependencyProperty.Register("HoverBorderBrush", typeof(Brush), typeof(IconButton),
				new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.AffectsMeasure | FrameworkPropertyMetadataOptions.AffectsRender, null, null));

			HoverBackgroundProperty = DependencyProperty.Register("HoverBackground", typeof(Brush), typeof(IconButton),
				new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.AffectsMeasure | FrameworkPropertyMetadataOptions.AffectsRender, null, null));

			IconProperty = DependencyProperty.Register("Icon", typeof(ControlTemplate), typeof(IconButton),
				new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.AffectsMeasure | FrameworkPropertyMetadataOptions.AffectsRender, null, null));

			IconWidthProperty = DependencyProperty.Register("IconWidth", typeof(double), typeof(IconButton),
				new FrameworkPropertyMetadata(0.0, FrameworkPropertyMetadataOptions.AffectsMeasure | FrameworkPropertyMetadataOptions.AffectsRender, null, null));

			IconHeightProperty = DependencyProperty.Register("IconHeight", typeof(double), typeof(IconButton),
				new FrameworkPropertyMetadata(0.0, FrameworkPropertyMetadataOptions.AffectsMeasure | FrameworkPropertyMetadataOptions.AffectsRender, null, null));
		}
	}
}
