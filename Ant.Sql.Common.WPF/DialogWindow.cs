﻿using System.Windows;
using System.Windows.Controls;

namespace Ant.Sql.Common.WPF
{
	[TemplatePart(Name = "PART_WindowCaption", Type = typeof(Grid))]
	public partial class DialogWindow : Window
	{
		static DialogWindow()
		{
			DefaultStyleKeyProperty.OverrideMetadata(typeof(DialogWindow), new FrameworkPropertyMetadata(typeof(DialogWindow)));

		}

		public DialogWindow() : base()
		{
			this.SetResourceReference(StyleProperty, typeof(DialogWindow));
			this.KeyDown += _keyDown;
			Loaded += DialogWindowBase_Loaded;
			WindowStartupLocation = WindowStartupLocation.CenterScreen;
			SetValue(TextBox.PaddingProperty, new Thickness(20));
		}

		private void _keyDown(object sender, System.Windows.Input.KeyEventArgs e)
		{
			if (e.Key == System.Windows.Input.Key.Escape)
			{
				Close();
			}
		}

		private void DialogWindowBase_Loaded(object sender, RoutedEventArgs e)
		{
			var ss = Template.FindName("PART_WindowCaption", this) as Grid;
			ss.MouseDown += Ss_MouseDown;
		}

		private void Ss_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			this.DragMove();
		}

		public override void OnApplyTemplate()
		{
			base.OnApplyTemplate();

			// gets called finally
		}
	}

}
