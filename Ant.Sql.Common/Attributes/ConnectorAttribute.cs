﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ant.Sql.Common.Attributes
{
	public class ConnectorAttribute : Attribute
	{
		public int V1 { get; private set; }
		public int V2 { get; private set; }
		public int V3 { get; private set; }
		public int V4 { get; private set; }

		public ConnectorAttribute(int v1, int v2, int v3, int v4)
		{

		}
	}
}
