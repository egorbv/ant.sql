﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Ant.Sql.Common.Wpf
{
	public class Command<T> : ICommand where T : class
	{
		Action<T> _executeAction;
		public event EventHandler CanExecuteChanged;

		public Command(Action<T> executeAction)
		{
			_executeAction = executeAction;
		}

		public bool CanExecute(object parameter)
		{
			return true;
		}

		

		public void Execute(object parameter)
		{
			_executeAction(parameter as T);
		}
	}
}
