﻿using System.Collections.Generic;

namespace Ant.Sql.Common.Management.Models
{
	/// <summary>
	/// Контекст для выполнения действия  в дереве обозревателя объектов
	/// </summary>
	public class TreeNodeContext
	{
		/// <summary>
		/// Текущий узел
		/// </summary>
		public TreeNode CurrentNode { get; set; }

		/// <summary>
		/// Список предков узла
		/// </summary>
		public List<TreeNode> Parents { get; set; }
	}
}
