﻿namespace Ant.Sql.Common.Management.Models
{
	/// <summary>
	/// Перечисление описывает комбинацию нажатых клавиш
	/// </summary>
	public enum KeyDownInfo
	{
		F5 = 1,
		Ctrl_R = 2,
		Delete = 3,
		Update = 4,
	}
}
