﻿namespace Ant.Sql.Common.Management.Models
{
	/// <summary>
	/// Моделе узла обозревателя объектов
	/// </summary>
	public class TreeNode
	{
		/// <summary>
		/// Название узла обозревателя объектов
		/// </summary>
		public string Name;

		/// <summary>
		/// Тип узла обозревателя объектов
		/// </summary>
		public TreeNodeType Type { get; private set; }

		/// <summary>
		/// Контсруктор
		/// </summary>
		/// <param name="type">Тип узла</param>
		/// <param name="name">Название узла</param>
		public TreeNode(TreeNodeType type, string name)
		{
			Type = type;
			switch (type)
			{
				case TreeNodeType.Databases:
					Name = "Базы данных";
					break;
				case TreeNodeType.Schemas:
					Name = "Схемы";
					break;
				case TreeNodeType.Functions:
					Name = "Функции";
					break;
				case TreeNodeType.Tables:
					Name = "Таблицы";
					break;
				case TreeNodeType.Sequences:
					Name = "Последовательности";
					break;
				case TreeNodeType.Securities:
					Name = "Пезопасность";
					break;
				case TreeNodeType.Users:
					Name = "Пользователи";
					break;
				case TreeNodeType.Roles:
					Name = "Роли";
					break;
				case TreeNodeType.Columns:
					Name = "Колонки";
					break;
				case TreeNodeType.Indexes:
					Name = "Индексы";
					break;
				case TreeNodeType.Rules:
					Name = "Правила";
					break;
				case TreeNodeType.Triggers:
					Name = "Триггеры";
					break;
				default:
					Name = name;
					break;
			}
		}
	}
}
