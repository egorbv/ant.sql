﻿using System;
using System.Collections.Generic;

namespace Ant.Sql.Common.Management.Models
{
	/// <summary>
	/// Модель элемента контекстного меню для дерева обозревателя объектов
	/// </summary>
	public class TreeContextMenuItem
	{
		/// <summary>
		/// Название эелемента контекстного меню
		/// </summary>
		public string Text { get; set; }

		/// <summary>
		/// Идентификатор действия
		/// </summary>
		public int ActionID { get; set; }

		public List<TreeContextMenuItem> Items { get; set; }
	}
}
