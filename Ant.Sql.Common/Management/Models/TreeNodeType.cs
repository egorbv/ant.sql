﻿namespace Ant.Sql.Common.Management.Models
{
	public enum TreeNodeType
	{
		Root = 0,
		ServerGroup = 1,
		ConnectorOffline = 2,
		ConnectorOnline = 3,

		Databases = 100,
		Database = 101,

		Tables = 200,
		Table = 201,
		Columns = 202,
		Indexes = 203,
		Rules = 204,
		Triggers = 205,

		Views = 300,
		View = 301,

		Schemas = 400,
		Schema = 401,

		Functions = 500,
		Function = 501,

		StoredProcedures = 600,
		StoredProcedure = 601,

		Sequences = 700,
		Sequence = 701,


		Securities = 800,
		Users = 801,
		User = 802,
		Roles = 803,
		Role = 804,



		Languages = 900,



		Custom = 65000,
		Folder = 65001,
	}
}
