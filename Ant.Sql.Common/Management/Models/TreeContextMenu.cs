﻿using System.Collections.Generic;

namespace Ant.Sql.Common.Management.Models
{
	/// <summary>
	/// Модель элементов контекстного меню для дерева обозревателя объектов
	/// </summary>
	public class TreeContextMenu
	{
		/// <summary>
		/// Список элементов контекстного меню
		/// </summary>
		public List<TreeContextMenuItem> Items { get; set; }

		public TreeContextMenu()
		{
			Items = new List<TreeContextMenuItem>();
		}
	}
}
