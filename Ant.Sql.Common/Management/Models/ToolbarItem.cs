﻿using System;

namespace Ant.Sql.Common.Management.Models
{
	/// <summary>
	/// Модель элемента тулбара
	/// </summary>
	public class ToolbarItem
	{
		public string Text { get; set; }
		public int ActionID { get; set; }
		public bool Disabed { get; set; }
		public int Key { get; set; }
	}
}
