﻿namespace Ant.Sql.Common.Management.Models
{
	/// <summary>
	/// Перечисление описывает результат выполнения действия дерева обозревателя объектов
	/// </summary>
	public enum TreeCommandType
	{
		/// <summary>
		/// Ничего делать не нужно
		/// </summary>
		None = 0,
		/// <summary>
		/// Обновить список потомков текущиего узла
		/// </summary>
		Refresh = 1,
		/// <summary>
		/// Обновить текущий узел
		/// </summary>
		Update = 2,
		/// <summary>
		/// Удалить текущий узел
		/// </summary>
		Delete = 3,
		/// <summary>
		/// Добавить новый узел к текущему узлу
		/// </summary>
		Create = 4,
		/// <summary>
		/// Добавить новый узел в корень дерева
		/// </summary>
		CreateInRoot = 5,
		/// <summary>
		/// Отсоедениться от сервера
		/// </summary>
		Disconnect = 6,
		/// <summary>
		/// Сохранить дополнительную информацию в узле
		/// </summary>
		SaveNodeData = 7,
	}
}
