﻿using System.Data.Common;
using System.Xml.Linq;

namespace Ant.Sql.Common.Management.Interfaces
{
	public interface IConnector
	{

		XElement Create();
		XElement Edit(XElement settings);
		IServerManager GetServerManager (XElement settings);
	}

	public interface IConnectionManager
	{
		DbConnection GetConnection();

		DbConnection GetConnection(string databaseName);
	}
}
