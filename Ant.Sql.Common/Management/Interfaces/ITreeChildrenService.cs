﻿using Ant.Sql.Common.Management.Models;
using System;
using System.Collections.Generic;

namespace Ant.Sql.Common.Management.Interfaces
{
	/// <summary>
	/// Интерфейс описывает сервис для получения данных
	/// для дерева обозревателя объектов
	/// </summary>
	public interface ITreeChildrenService
	{
		List<TreeNode> Get(TreeNodeContext context);
		TreeContextMenu GetContextMenu(TreeNodeContext context);
		void ExecuteTreeAction(TreeNodeContext context, int actionID);
	}
}
