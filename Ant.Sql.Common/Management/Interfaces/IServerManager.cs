﻿using Ant.Sql.Common.Management.Models;
using System;
using System.Collections.Generic;

namespace Ant.Sql.Common.Management.Interfaces
{
	public interface IServerManager
	{
		ITreeChildrenService TreeChildrenService { get; }
		ITreeKeyboardService TreeKeyboardService { get; }
		ITreeToolbarService TreeToolbarService { get; }

		void Initializate(IConnectionManager connectionManager);

		/// <summary>
		/// Выыполнить команду панели инструментов менеджера сервера
		/// </summary>
		/// <param name="actionID">Идентификатор деуствия</param>
		/// <returns>Результат исполнения действия</returns>
		void ExecuteToolbarAction(TreeNodeContext context, int actionID);

		List<ToolbarItem> GetToolbarItemsItems();
	}
}
