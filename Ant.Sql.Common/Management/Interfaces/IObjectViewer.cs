﻿using Ant.Sql.Common.Management.Models;
using System;
using System.Collections.Generic;

namespace Ant.Sql.Common.Management.Interfaces
{
	public interface IObjectViewer
	{
	}


	public interface IObjectViewerElement
	{
		List<ToolbarItem> GetToolbarItems();
		void Execute(int actionID, object data);
	}
}
