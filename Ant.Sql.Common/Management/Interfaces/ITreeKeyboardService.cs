﻿using Ant.Sql.Common.Management.Models;

namespace Ant.Sql.Common.Management.Interfaces
{
	/// <summary>
	/// Интерфейс описывает сервис для работы с клавиатурой в дереве обозревателя объектов
	/// </summary>
	public interface ITreeKeyboardService
	{
		/// <summary>
		/// Выполнить действие
		/// </summary>
		/// <param name="context">Контекст текущего узла в дереве обозревателя</param>
		/// <param name="keyDownInfo">Сочетание нажатых клавиш</param>
		void Execute(TreeNodeContext context, KeyDownInfo keyDownInfo);
	}
}
