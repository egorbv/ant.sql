﻿using Ant.Sql.Common.Management.Models;
using System;
using System.Collections.Generic;

namespace Ant.Sql.Common.Management.Interfaces
{
	/// <summary>
	/// Интерфейс описывает сервис для обслуживания тулбара над обозревателем объектов сервера
	/// </summary>
	public interface ITreeToolbarService
	{
		/// <summary>
		/// Выполнить команду меню
		/// </summary>
		/// <param name="context">Контекст текущего узла в дереве обозревателя</param>
		/// <param name="actionID">Идентификатор деуствия</param>
		void Execute(TreeNodeContext context, int actionID);

		/// <summary>
		/// Получить список эделентов для тулбара дерева обозревателя объектов
		/// </summary>
		/// <param name="node">Текущий узел в дереве</param>
		/// <returns>Список элементов</returns>
		List<ToolbarItem> GetItems(TreeNode node);

	}
}
