﻿using System;
using System.Collections.Generic;

namespace Ant.Sql.Common.Management
{
	public static class TriggerManager
	{
		static object _sync;
		static Dictionary<Type, List<Action<object>>> _triggers;

		static TriggerManager()
		{
			_sync = new object();
			_triggers = new Dictionary<Type, List<Action<object>>>();
		}

		public static void Subscribe<T,T1>(Action<T1> action)  where T1 : class
		{
			lock(_sync)
			{
				if(!_triggers.ContainsKey(typeof(T)))
				{
					_triggers.Add(typeof(T), new List<Action<object>>());
				}
				_triggers[typeof(T)].Add((x)=> { action(x as T1); });
			}
		}

		public static void Fire<T>(object args)
		{
			lock(_sync)
			{
				foreach(var item in _triggers[typeof(T)])
				{
					item(args);
				}
			}
		}
	}
}
