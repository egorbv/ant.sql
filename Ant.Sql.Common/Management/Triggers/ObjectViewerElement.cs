﻿using Ant.Sql.Common.Management.Interfaces;

namespace Ant.Sql.Common.Management.Triggers
{
	public delegate void ObjectViewerElement(ObjectViewerElementArgs args);

	public class ObjectViewerElementArgs
	{
		public IObjectViewerElement Element { get; set; }
		public string Header { get; set; }
	}
}
