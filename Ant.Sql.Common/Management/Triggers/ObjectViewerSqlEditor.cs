﻿using Ant.Sql.Common.Management.Interfaces;

namespace Ant.Sql.Common.Management.Triggers
{
	public delegate void ObjectViewerSqlEditor(ObjectViewerSqlEditorArgs args);

	public class ObjectViewerSqlEditorArgs
	{
		public IObjectViewerElement Element { get; set; }
		public string Script { get; set; }
	}

}
