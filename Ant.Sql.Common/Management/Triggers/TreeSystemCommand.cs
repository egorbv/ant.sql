﻿using Ant.Sql.Common.Management.Models;

namespace Ant.Sql.Common.Management.Triggers
{
	public delegate void TreeSystemCommand();

	public class TreeSystemCommandArgs
	{
		public TreeCommandType CommandType{get; set;}
		public TreeNode Node { get; set; }
		public object ExtData { get; set; }
	}
}
