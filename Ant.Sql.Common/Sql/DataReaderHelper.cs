﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Data.Common
{
	public static class DataReaderHelper
	{
		public static T GetValue<T>(this IDataReader reader, string fieldName)
		{
			var value = reader[fieldName];
			if (value is DBNull)
			{
				return default(T);
			}
			return (T)value;
		}

		//public static T GetValue<T>(this DbDataReader reader, string fieldName)
		//{
		//	var value = reader[fieldName];
		//	if (value is DBNull)
		//	{
		//		return default(T);
		//	}
		//	return (T)value;
		//}
	}
}
