﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Test
{
	/// <summary>
	/// Interaction logic for DockManager.xaml
	/// </summary>
	public partial class DockManager : UserControl
	{
		List<UserControl> _panels;

		public DockManager()
		{
			InitializeComponent();
			_panels = new List<UserControl>();
			var btn = new Button() { Content = "Wdrwrew" };
			btn.SetValue(Grid.RowProperty, 2);
			btn.SetValue(Grid.ColumnProperty, 2);
			MainGrid.Children.Add(btn);
		}

		public void AddDock(UserControl control, TT tt)
		{
			var docContainer = new DockPanelContainer(control, tt,this);
			if (tt == TT.RightResize)
			{
				docContainer.SetValue(Grid.RowSpanProperty, 5);
			}
			if(tt == TT.LeftResize)
			{
				docContainer.SetValue(Grid.RowSpanProperty, 5);
				docContainer.SetValue(Grid.ColumnProperty, 4);
			}


			_panels.Add(docContainer);
			MainGrid.Children.Add(docContainer);
		}
	}
}
