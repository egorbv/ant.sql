﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Test
{
	/// <summary>
	/// Interaction logic for DockPanelContainer.xaml
	/// </summary>
	public partial class DockPanelContainer : UserControl
	{
		UserControl _content;
		UserControl _docManager;
		double _startXPosition;
		double _startWidth;

		public DockPanelContainer()
		{
			InitializeComponent();
		}

		public DockPanelContainer(UserControl control, TT type, UserControl docManager)
		{
			InitializeComponent();
			switch (type)
			{
				case TT.RightResize:
					MainBorder.BorderThickness = new Thickness(0, 0, 3, 0);
					break;
			}

			MainBorder.Child = control;
			_content = control;
			_docManager = docManager;
		}

		private void DocManager_MouseMove(object sender, MouseEventArgs e)
		{
			if (_mouseDown)
			{
				var x = Mouse.GetPosition(_docManager).X;
				if (_startXPosition < x)
				{
					_content.Width = _startWidth + x - _startXPosition;
				}
			}
		}

		bool _mouseDown;
		private void MainBorder_MouseDown(object sender, MouseButtonEventArgs e)
		{
			_mouseDown = true;
			_startXPosition = Mouse.GetPosition(_docManager).X;
			_startWidth = _content.ActualWidth;
			_docManager.MouseMove += DocManager_MouseMove;
		}

		private void MainBorder_MouseUp(object sender, MouseButtonEventArgs e)
		{
			_mouseDown = false;
			Mouse.Capture(null);
			_docManager.MouseMove -= DocManager_MouseMove;
		}
	}


	public enum TT
	{
		LeftResize,
		RightResize,
		TopResize,
		BottomResize,
	}
}
