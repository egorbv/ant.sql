﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Test.Controls
{
	public class DockContainer : Grid
	{
		Grid _parent;
		ColumnDefinition _centerColumm;
		RowDefinition _centerRow;
		bool _mousePressed;
		double _startXPos;
		double _startYPos;
		double _startWidth;
		double _startHeight;

		public DockContainerResizeMode ResizeMode { get; set; }
		public int CenterPosition { get; set; }

		public DockContainer()
		{
			this.Loaded += DockContainer_Loaded;
			this.MouseUp += _mouseUpHandler;
		}

		private void DockContainer_Loaded(object sender, System.Windows.RoutedEventArgs e)
		{
			_parent = Parent as Grid;
			_centerColumm = _parent.ColumnDefinitions[CenterPosition];
			_centerRow = _parent.RowDefinitions[CenterPosition];
			var button = new Grid() { Background = new SolidColorBrush( Colors.Green) };
			button.MouseDown += _mouseDownHandler;
			switch (ResizeMode)
			{
				case DockContainerResizeMode.LeftToRight:
					button.Cursor = Cursors.SizeWE;
					ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(0.1, GridUnitType.Star) });
					ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(5, GridUnitType.Pixel) });
					Children.Add(button);
					SetColumn(button, 1);
					break;
				case DockContainerResizeMode.RightToLeft:
					button.Cursor = Cursors.SizeWE;
					ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(5, GridUnitType.Pixel) });
					ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(0.1, GridUnitType.Star) });
					Children.Add(button);
					SetColumn(button, 0);
					break;
				case DockContainerResizeMode.TopToBottom:
					button.Cursor = Cursors.SizeNS;
					RowDefinitions.Add(new RowDefinition() { Height = new GridLength(0.1, GridUnitType.Star) });
					RowDefinitions.Add(new RowDefinition() { Height = new GridLength(5, GridUnitType.Pixel) });
					Children.Add(button);
					SetRow(button, 1);
					break;
				case DockContainerResizeMode.BottomToTop:
					button.Cursor = Cursors.SizeNS;
					RowDefinitions.Add(new RowDefinition() { Height = new GridLength(5, GridUnitType.Pixel) });
					RowDefinitions.Add(new RowDefinition() { Height = new GridLength(0.1, GridUnitType.Star) });
					Children.Add(button);
					SetRow(button, 0);
					break;
			}


		}


		double _getWidth(double delta)
		{
			var centerWidth = _centerColumm.ActualWidth;
			if (centerWidth - delta < 50)
			{
				delta = centerWidth - 50;
			}
			return ActualWidth + delta;
		}
		double _getHeight(double delta)
		{
			var centerHieght = _centerRow.ActualHeight;
			if (centerHieght - delta < 50)
			{
				delta = centerHieght - 50;
			}
			return ActualHeight + delta;
		}

		private void _LeftToRightMouseMoveHandler(object sender, System.Windows.Input.MouseEventArgs e)
		{
			if(_mousePressed)
			{
				var x = Mouse.GetPosition(App.Current.MainWindow).X;
				if (_startXPos < x)
				{
					Width = _getWidth( _startWidth + x - _startXPos - ActualWidth);

				}
				else
				{
					var width = _startWidth-( _startXPos - x);
					if(width < 20)
					{
						width = 20;
					}
					Width = width;
				}
			}
		}

		private void _RightToLeftMouseMoveHandler(object sender, System.Windows.Input.MouseEventArgs e)
		{
			if (_mousePressed)
			{
				var x = Mouse.GetPosition(App.Current.MainWindow).X;
				if (_startXPos > x)
				{
					Width = _getWidth( _startWidth + _startXPos - x - ActualWidth);
				}
				else
				{
					var width = _startWidth - ( x - _startXPos);
					if (width < 20)
					{
						width = 20;
					}
					Width = width;
				}
			}
		}

		private void _TopToBottomMouseMoveHandler(object sender, System.Windows.Input.MouseEventArgs e)
		{
			if (_mousePressed)
			{
				var y = Mouse.GetPosition(App.Current.MainWindow).Y;
				if (_startYPos < y)
				{
					Height = _getHeight(_startHeight + y - _startYPos - ActualHeight);
				}
				else
				{
					var height = _startHeight - (_startYPos - y);
					if (height < 20)
					{
						height = 20;
					}
					Height = height;
				}
			}
		}

		private void _BottomToTopMouseMoveHandler(object sender, System.Windows.Input.MouseEventArgs e)
		{
			if (_mousePressed)
			{
				var y = Mouse.GetPosition(App.Current.MainWindow).Y;
				if (_startYPos > y)
				{
					Height = _getHeight(  _startHeight + _startYPos - y - ActualHeight);
				}
				else
				{
					var height = _startHeight - (y - _startYPos);
					if (height < 20)
					{
						height = 20;
					}
					Height = height;
				}
			}
		}

		private void _mouseDownHandler(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			Cursor = (sender as Grid).Cursor;
			CaptureMouse();
			_mousePressed = true;
			var pos = Mouse.GetPosition(App.Current.MainWindow);
			_startXPos = pos.X;
			_startYPos = pos.Y;
			_startWidth = ActualWidth;
			_startHeight = ActualHeight;
			switch (ResizeMode)
			{
				case DockContainerResizeMode.LeftToRight:
					App.Current.MainWindow.MouseMove += _LeftToRightMouseMoveHandler;
					break;
				case DockContainerResizeMode.RightToLeft:
					App.Current.MainWindow.MouseMove += _RightToLeftMouseMoveHandler;
					break;
				case DockContainerResizeMode.TopToBottom:
					App.Current.MainWindow.MouseMove += _TopToBottomMouseMoveHandler;
					break;

				case DockContainerResizeMode.BottomToTop:
					App.Current.MainWindow.MouseMove += _BottomToTopMouseMoveHandler;
					break;
			}
		}

		private void _mouseUpHandler(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			_mousePressed = false;
			switch (ResizeMode)
			{
				case DockContainerResizeMode.LeftToRight:
					App.Current.MainWindow.MouseMove -= _LeftToRightMouseMoveHandler;
					break;
				case DockContainerResizeMode.RightToLeft:
					App.Current.MainWindow.MouseMove -= _RightToLeftMouseMoveHandler;
					break;
				case DockContainerResizeMode.TopToBottom:
					App.Current.MainWindow.MouseMove -= _TopToBottomMouseMoveHandler;
					break;

				case DockContainerResizeMode.BottomToTop:
					App.Current.MainWindow.MouseMove -= _BottomToTopMouseMoveHandler;
					break;
			}
			ReleaseMouseCapture();
		}
	}
}
