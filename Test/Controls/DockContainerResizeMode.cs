﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Controls
{
	public enum DockContainerResizeMode
	{
		LeftToRight,
		RightToLeft,
		TopToBottom,
		BottomToTop,
	}
}
