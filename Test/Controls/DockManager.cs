﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Test.Controls
{
	public class DockManager : Grid
	{
		DockContainer _firstLeftDockContainer;
		DockContainer _secondLeftDockContainer;
		DockContainer _firstRightDockContainer;
		DockContainer _secondRightDockContainer;

		DockContainer _firstTopDockContainer;
		DockContainer _secondTopDockContainer;
		DockContainer _firstBottomDockContainer;
		DockContainer _secondBottomDockContainer;

		public DockManager()
		{
			RowDefinitions.Add(new RowDefinition() { Height = new GridLength(0.1, GridUnitType.Auto)});
			RowDefinitions.Add(new RowDefinition() { Height = new GridLength(0.1, GridUnitType.Auto) });
			RowDefinitions.Add(new RowDefinition() { Height = new GridLength(0.1, GridUnitType.Star) });
			RowDefinitions.Add(new RowDefinition() { Height = new GridLength(0.1, GridUnitType.Auto) });
			RowDefinitions.Add(new RowDefinition() { Height = new GridLength(0.1, GridUnitType.Auto) });

			ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(0.1, GridUnitType.Auto) });
			ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(0.1, GridUnitType.Auto) });
			ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(0.1, GridUnitType.Star) });
			ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(0.1, GridUnitType.Auto) });
			ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(0.1, GridUnitType.Auto) });

			_firstLeftDockContainer = new DockContainer() { Width = 200, ResizeMode = DockContainerResizeMode.LeftToRight, CenterPosition = 2 ,Visibility = Visibility.Collapsed };
			_firstRightDockContainer = new DockContainer() { Width = 200, ResizeMode = DockContainerResizeMode.RightToLeft, CenterPosition = 2 };
			_firstTopDockContainer = new DockContainer() { Height = 200, ResizeMode = DockContainerResizeMode.TopToBottom , CenterPosition = 2};
			_firstBottomDockContainer = new DockContainer() { Height = 200, ResizeMode = DockContainerResizeMode.BottomToTop, CenterPosition =  2 };
			Children.Add(_firstBottomDockContainer);
			Children.Add(_firstLeftDockContainer);
			Children.Add(_firstTopDockContainer);
			Children.Add(_firstRightDockContainer);
			_setPosition(false);
				
		}

		protected override void OnVisualChildrenChanged(DependencyObject visualAdded, DependencyObject visualRemoved)
		{
		}

		public void _setPosition(bool fl)
		{
			if(fl)
			{
				SetRowSpan(_firstLeftDockContainer, 5);
				SetColumn(_firstLeftDockContainer, 0);
				SetRow(_firstLeftDockContainer, 0);

				SetRowSpan(_firstRightDockContainer, 5);
				SetColumn(_firstRightDockContainer, 4);
				SetRow(_firstRightDockContainer, 0);

				SetColumnSpan(_firstTopDockContainer, 1);
				SetRow(_firstTopDockContainer, 0);
				SetColumn(_firstTopDockContainer, 2);

				SetColumnSpan(_firstBottomDockContainer, 1);
				SetColumn(_firstBottomDockContainer, 2);
				SetRow(_firstBottomDockContainer, 4);
				

			}
			else
			{
				SetRowSpan(_firstLeftDockContainer, 1);
				SetColumn(_firstLeftDockContainer, 0);
				SetRow(_firstLeftDockContainer, 2);

				SetRowSpan(_firstRightDockContainer, 1);
				SetColumn(_firstRightDockContainer, 4);
				SetRow(_firstRightDockContainer, 2);

				SetColumnSpan(_firstTopDockContainer, 5);
				SetRow(_firstTopDockContainer, 0);
				SetColumn(_firstTopDockContainer, 0);

				SetColumnSpan(_firstBottomDockContainer, 5);
				SetColumn(_firstBottomDockContainer, 0);
				SetRow(_firstBottomDockContainer, 4);

			}
		}
	}
}
