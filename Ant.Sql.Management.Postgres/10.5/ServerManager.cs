﻿using System;
using System.Collections.Generic;
using Ant.Sql.Common.Management.Interfaces;
using Ant.Sql.Common.Management.Models;
using Ant.Sql.Management.Postgres._10._5.ServerManagerActions;
using Ant.Sql.Management.Postgres._10._5.Services;

namespace Ant.Sql.Management.Postgres._10._5
{
	public class ServerManager : IServerManager
	{
		protected object sync;
		protected IConnectionManager connectionManager;
		protected IObjectViewer objectViewer;

		protected List<ToolbarItem> toolbarItems;
		protected Dictionary<int, IServerManagerAction> actions;
		protected int ActionCreateScript =  1;

		public ITreeChildrenService TreeChildrenService { get; protected set; }
		public ITreeKeyboardService TreeKeyboardService { get; protected set; }
		public ITreeToolbarService TreeToolbarService { get; protected set; }

		#region ExecuteToolbarAction
		/// <summary>
		/// Выполняет команда тулбара сервера
		/// </summary>
		/// <param name="actionID">Идентификатор действия которое необходимо выполнить</param>
		public void ExecuteToolbarAction(TreeNodeContext context, int actionID)
		{
			if(actions== null)
			{
				lock(sync)
				{
					if(actions == null)
					{
						createActions();
					}
				}
			}
			var schemaInfo = context.GetSchemaInfo();
			actions[actionID].Execute(schemaInfo);
		}
		#endregion

		#region GetToolbarItemsItems
		/// <summary>
		/// Получить список доступных элементов для тулбара сервера
		/// </summary>
		/// <returns>Список элементов</returns>
		public List<ToolbarItem> GetToolbarItemsItems()
		{
			if(toolbarItems == null)
			{
				lock(sync)
				{
					if(toolbarItems == null)
					{
						createToolbarItems();
					}
				}
			}
			return toolbarItems;
		}
		#endregion

		public virtual void Initializate(IConnectionManager connectionManager)
		{
			this.connectionManager = connectionManager;
			sync = new object();
			TreeChildrenService = new TreeChildrenService(connectionManager);
			TreeToolbarService = new TreeToolbarService(this.TreeChildrenService);
		}

		#region createActions
		/// <summary>
		/// Создает список обработчиков тулбара сервера
		/// </summary>
		protected virtual void createActions()
		{
			actions = new Dictionary<int, IServerManagerAction>
			{
				[ActionCreateScript] = new CreateScript(connectionManager)
			};
		}
		#endregion

		#region createToolbarItems
		/// <summary>
		/// Создает доступных эелементов для тулбара сервера
		/// </summary>
		protected virtual void createToolbarItems()
		{
			toolbarItems = new List<ToolbarItem>
			{
				new ToolbarItem() { Text = "Создать запрос", ActionID = ActionCreateScript }
			};
		}
		#endregion
	}
}
