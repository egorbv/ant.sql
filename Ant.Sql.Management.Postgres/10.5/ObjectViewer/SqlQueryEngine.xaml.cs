﻿using Ant.Sql.Common;
using Ant.Sql.Common.Management.Interfaces;
using Ant.Sql.Common.Management.Models;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;

namespace Ant.Sql.Management.Postgres._10._5.ObjectViewer
{
	public partial class SqlQueryEngine : UserControl, IObjectViewerElement
	{
		IConnectionManager _connectionManager;
		DbConnection _connection;
		string _databaseName;
		List<ToolbarItem> _toolbarItems;
		protected int ExecuteCommand = 1;


		public SqlQueryEngine(IConnectionManager connectionManager, string databaseName)
		{
			InitializeComponent();
			_connectionManager = connectionManager;
			Visibility = Visibility.Collapsed;
			_toolbarItems = new List<ToolbarItem>()
			{
				new ToolbarItem() { Text = "Выполнить" , ActionID = ExecuteCommand, Key = (int)Key.F5 }
			};
			_databaseName = databaseName;
		}

		public List<ToolbarItem> GetToolbarItems()
		{
			return _toolbarItems;
		}

		public void Execute(int actionID, object data)
		{
			if(actionID == ExecuteCommand)
			{
				_executeScript(data as string);
			}
		}

		void _executeScript(string script)
		{
			if(_connection == null)
			{
				_connection = _connectionManager.GetConnection(_databaseName);
			}
			using (var command = _connection.CreateCommand())
			{
				command.CommandText = script;
				try
				{
					command.Prepare();
					using (var reader = command.ExecuteReader())
					{
						DataPanel.Children.Clear();
						_readTables(reader);
						Visibility = Visibility.Visible;
					}
				}
				catch(Exception e)
				{
					MessageBox.Show(e.Message);
				}
			}
		}

		void _readTables(DbDataReader reader)
		{
			var grid = new DataGrid();
			grid.MaxHeight = 400.0;
			grid.AutoGenerateColumns = false;
			for(var i=0; i < reader.FieldCount; i++)
			{
				var column = new DataGridTextColumn() { Header = reader.GetName(i) };
				column.Binding = new Binding($"Cells[{i}]");
				column.Width = 100;
				grid.Columns.Add(column);

			}
			var rows = new List<Row>();
			while (reader.Read())
			{
				var row = new Row();
				for (var i = 0; i < reader.FieldCount; i++)
				{
					row.Cells.Add(reader.GetValue(i));
				}
				rows.Add(row);
			}
			grid.ItemsSource = rows;
			grid.RowHeaderWidth = 20;
			grid.CanUserAddRows = false;
			DataPanel.Children.Add(grid);
			if (reader.NextResult())
			{
				_readTables(reader);
			}
		}

		public class Row
		{
			public List<object> Cells { get; set; }
			public Row()
			{
				Cells = new List<object>();
			}
		}
	}
}
