﻿using Ant.Sql.Common.Management.Interfaces;
using Ant.Sql.Common.Management.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using static Ant.Sql.Management.Postgres._10._5.TreeNodeManagers.TreeNodeManagerBase;

namespace Ant.Sql.Management.Postgres._10._5.ObjectViewer
{
	/// <summary>
	/// Interaction logic for Report.xaml
	/// </summary>
	public partial class Report : UserControl, IObjectViewerElement
	{
		List<ToolbarItem> _toolbarItems;
		IConnectionManager _connectionManager;
		SchemaInfo _schemaInfo;
		string _script;
		int RefreshCommand = 1;

		public Report(IConnectionManager connectionManager, SchemaInfo schemaInfo, string script)
		{
			InitializeComponent();
			_toolbarItems = new List<ToolbarItem>();
			_toolbarItems.Add(new ToolbarItem() { Text = "Обновить", ActionID = RefreshCommand });
			_script = script;
			_schemaInfo = schemaInfo;
			_connectionManager = connectionManager;
			Execute(RefreshCommand, null);
		}

		public async void Execute(int actionID, object data)
		{
			var result = new List<Row>();
			MainGrid.Cursor = Cursors.Wait;
			MainGrid.ItemsSource = null;
			var columns = new List<string>();
			await Task.Run(() =>
			{
				using (var connection = _connectionManager.GetConnection(_schemaInfo.Database))
				{
					using (var command = connection.CreateCommand())
					{
						command.CommandText = _script;
						using (var reader = command.ExecuteReader())
						{
							if (MainGrid.Columns.Count == 0)
							{
								for (var i = 0; i < reader.FieldCount; i++)
								{
									columns.Add(reader.GetName(i));
								}
							}
							while (reader.Read())
							{
								var row = new Row() { Cells = new object[reader.FieldCount] };
								reader.GetValues(row.Cells);
								result.Add(row);
							}
						}

					}
				}
			});
			
			for(var i=0; i < columns.Count; i++)
			{
				var textColumn = new DataGridTextColumn() { Header = columns[i] };
				textColumn.Binding = new Binding($"Cells[{i}]");
				textColumn.Width = 100;
				MainGrid.Columns.Add(textColumn);
			}
			MainGrid.Cursor = Cursors.Arrow;
			MainGrid.ItemsSource = result;
		}

		public List<ToolbarItem> GetToolbarItems()
		{
			return _toolbarItems;
		}
	}
	public class Row
	{
		public object[] Cells { get; set; }
		public Row()
		{
		}
	}
}
