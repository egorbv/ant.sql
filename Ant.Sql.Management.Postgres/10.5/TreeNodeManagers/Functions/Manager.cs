﻿using System;
using System.Data.Common;
using System.Collections.Generic;
using Ant.Sql.Common.Management.Interfaces;
using Ant.Sql.Common.Management.Models;

namespace Ant.Sql.Management.Postgres._10._5.TreeNodeManagers.Functions
{
	public class Manager : TreeNodeManagerBase
	{
		public override List<TreeNode> GetChildren(IConnectionManager connectionManager, TreeNodeContext context)
		{
			var schemaInfo = context.GetSchemaInfo();
			var result = new List<TreeNode>();
			using (var connection = connectionManager.GetConnection(schemaInfo.Database))
			{
				using (var command = connection.CreateCommand())
				{
					command.CommandText = string.Format("SELECT routine_name FROM information_schema.routines WHERE routines.specific_schema='{0}'", schemaInfo.Schema);
					using (var reader = command.ExecuteReader())
					{
						while (reader.Read())
						{
							var functionName = reader.GetValue<string>("routine_name");
							result.Add(new TreeNode(TreeNodeType.Function, functionName));
						}
					}
				}
			}
			return result;
		}

		protected override void createContextMenuHandlers(IConnectionManager connectionManager, TreeNodeContext context)
		{
		}
	}
}
