﻿using System.Collections.Generic;
using Ant.Sql.Common.Management.Interfaces;
using Ant.Sql.Common.Management.Models;

namespace Ant.Sql.Management.Postgres._10._5.TreeNodeManagers.Schema
{
	public class Manager : TreeNodeManagerBase
	{
		public override List<TreeNode> GetChildren(IConnectionManager connectionManager, TreeNodeContext context)
		{
			var result = new List<TreeNode>
			{
				new TreeNode(TreeNodeType.Functions, null),
				new TreeNode(TreeNodeType.Tables, null),
				new TreeNode(TreeNodeType.Sequences, null)
			};
			return result;
		}

		protected override void createContextMenuHandlers(IConnectionManager connectionManager, TreeNodeContext context)
		{
		}
	}
}
