﻿using static Ant.Sql.Management.Postgres._10._5.TreeNodeManagers.TreeNodeManagerBase;

namespace Ant.Sql.Management.Postgres._10._5.TreeNodeManagers
{
	public interface ITreeNodeAction
	{
		void Execute(SchemaInfo schemaInfo);
	}
}
