﻿using System.Collections.Generic;
using Ant.Sql.Common.Management.Interfaces;
using Ant.Sql.Common.Management.Models;

namespace Ant.Sql.Management.Postgres._10._5.TreeNodeManagers.Table
{
	public class Manager : TreeNodeManagerBase
	{
		public override List<TreeNode> GetChildren(IConnectionManager connectionManager, TreeNodeContext context)
		{
			return new List<TreeNode>
			{
				new TreeNode(TreeNodeType.Columns, null),
				new TreeNode(TreeNodeType.Indexes, null),
				new TreeNode(TreeNodeType.Triggers, null),
				new TreeNode(TreeNodeType.Rules, null),
			};
		}

		protected override TreeContextMenu getContextMenu(IConnectionManager connectionManager, TreeNodeContext context)
		{
			var result = new TreeContextMenu();
			result.Items.Add(new TreeContextMenuItem() { Text = "Обновить", ActionID = TreeActionType.SystemRefresh });
			result.Items.Add(new TreeContextMenuItem() { Text = "Просмотр первых 2000 записией" });
			result.Items.Add(new TreeContextMenuItem() { Text = "Редактирование первых 200 записией" });
			result.Items.Add(new TreeContextMenuItem() { Text = "Удалить", ActionID = NodeActions.TableDelete });

			var scripts = new TreeContextMenuItem() { Text = "Скрипты", Items = new List<TreeContextMenuItem>() };
			scripts.Items.Add(new TreeContextMenuItem() { Text = "Скрипт CREATE", ActionID = NodeActions.ScriptCreate });
			scripts.Items.Add(new TreeContextMenuItem() { Text = "Скрипт SELECT", ActionID = NodeActions.ScriptSelect });
			scripts.Items.Add(new TreeContextMenuItem() { Text = "Скрипт INSERT", ActionID = NodeActions.ScriptInsert });
			scripts.Items.Add(new TreeContextMenuItem() { Text = "Скрипт UPDATE", ActionID = NodeActions.ScriptUpdate });
			scripts.Items.Add(new TreeContextMenuItem() { Text = "Скрипт DELETE", ActionID = NodeActions.ScriptDelete });
			result.Items.Add(scripts);
			result.Items.Add(new TreeContextMenuItem() { Text = "Свойства" });
			return result;
		}

		protected override void createContextMenuHandlers(IConnectionManager connectionManager, TreeNodeContext context)
		{
			actions = new Dictionary<int, ITreeNodeAction>
			{
				[NodeActions.ScriptSelect] = new ScriptSelect(connectionManager),
				[NodeActions.ScriptUpdate] = new ScriptUpdate(connectionManager),
				[NodeActions.ScriptInsert] = new ScriptInsert(connectionManager),
				[NodeActions.ScriptDelete] = new ScriptDelete(connectionManager),
				[NodeActions.ScriptCreate] = new ScriptCreate(connectionManager),

				[NodeActions.TableDelete] = new TableDelete(connectionManager)
			};
		}
	}
}
