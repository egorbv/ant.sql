﻿using Ant.Sql.Common.Management.Interfaces;
using Ant.Sql.Common.WPF;
using System;
using System.Windows;
using static Ant.Sql.Management.Postgres._10._5.TreeNodeManagers.TreeNodeManagerBase;

namespace Ant.Sql.Management.Postgres._10._5.TreeNodeManagers.Table
{
	public class TableDelete : ITreeNodeAction
	{
		IConnectionManager _connectionManager;

		public TableDelete(IConnectionManager connectionManager)
		{
			_connectionManager = connectionManager;
		}

		public void Execute(SchemaInfo schemaInfo)
		{
			var dialog = new DropObject($"Вы уверены что хотите удалить таблицу \"{schemaInfo.Schema}.{schemaInfo.NodeName}\" ?");
			if (dialog.ShowDialog() == true)
			{
				try
				{
					using (var connection = _connectionManager.GetConnection(schemaInfo.Database))
					{
						using (var command = connection.CreateCommand())
						{
							command.CommandText = $"DROP TABLE {schemaInfo.Schema}.\"{schemaInfo.NodeName}\"";
							command.ExecuteNonQuery();
						}
					}
				}
				catch(Exception e)
				{

				}
			}
		}

	}
}
