﻿using Ant.Sql.Common.Management;
using Ant.Sql.Common.Management.Interfaces;
using Ant.Sql.Common.Management.Triggers;
using Ant.Sql.Management.Postgres._10._5.ObjectViewer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Ant.Sql.Management.Postgres._10._5.TreeNodeManagers.TreeNodeManagerBase;

namespace Ant.Sql.Management.Postgres._10._5.TreeNodeManagers.Table
{
	public class SelectTable : ITreeNodeAction
	{
		IConnectionManager _connectionManager;

		public SelectTable(IConnectionManager connectionManager)
		{
			_connectionManager = connectionManager;
		}

		public void Execute(SchemaInfo schemaInfo)
		{
			var text = $"SELECT * FROM {schemaInfo.Schema}.\"{schemaInfo.NodeName}\"";
			var scriptEditor = new SqlQueryEngine(_connectionManager, schemaInfo.Database);
			var args = new ObjectViewerSqlEditorArgs() { Script = text, Element = scriptEditor };
			TriggerManager.Fire<ObjectViewerSqlEditor>(args);
		}

	}
}
