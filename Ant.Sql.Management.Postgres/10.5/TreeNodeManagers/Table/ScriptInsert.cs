﻿using Ant.Sql.Common.Management;
using Ant.Sql.Common.Management.Interfaces;
using Ant.Sql.Common.Management.Triggers;
using Ant.Sql.Management.Postgres._10._5.ObjectViewer;
using static Ant.Sql.Management.Postgres._10._5.TreeNodeManagers.TreeNodeManagerBase;
using System.Data.Common;
using System.Text;

namespace Ant.Sql.Management.Postgres._10._5.TreeNodeManagers.Table
{
	public class ScriptInsert : ITreeNodeAction
	{
		IConnectionManager _connectionManager;

		public ScriptInsert(IConnectionManager connectionManager)
		{
			_connectionManager = connectionManager;
		}

		public void Execute(SchemaInfo schemaInfo)
		{
			var f = false;
			var columnCount = -1;
			var sb = new StringBuilder();
			sb.Append($"INSERT INTO {schemaInfo.Schema}.\"{schemaInfo.NodeName}\"\r\n\t\t(");
			using (var connection = _connectionManager.GetConnection(schemaInfo.Database))
			{
				using (var command = connection.CreateCommand())
				{
					command.CommandText = $"SELECT column_name FROM information_schema.COLUMNS WHERE table_name = '{schemaInfo.NodeName}' and table_schema = '{schemaInfo.Schema}' order by ordinal_position;";
					using (var reader = command.ExecuteReader())
					{
						while (reader.Read())
						{
							if (f)
							{
								sb.Append(", ");
							}
							sb.Append($"\"{reader.GetValue<string>("column_name")}\"");
							f = true;
							columnCount++;
						}
					}
				}
			}
			sb.Append(")\r\n\tVALUES\r\n\t\t(");
			for(var i=0; i < columnCount; i++)
			{
				sb.Append("?, ");
			}
			sb.Append("?)");
			var scriptEditor = new SqlQueryEngine(_connectionManager, schemaInfo.Database);
			var args = new ObjectViewerSqlEditorArgs() { Script = sb.ToString(), Element = scriptEditor };
			TriggerManager.Fire<ObjectViewerSqlEditor>(args);
		}
	}
}
