﻿using Ant.Sql.Common.Management;
using Ant.Sql.Common.Management.Interfaces;
using Ant.Sql.Common.Management.Triggers;
using Ant.Sql.Management.Postgres._10._5.ObjectViewer;
using static Ant.Sql.Management.Postgres._10._5.TreeNodeManagers.TreeNodeManagerBase;
using System.Data.Common;
using System.Text;

namespace Ant.Sql.Management.Postgres._10._5.TreeNodeManagers.Table
{
	public class ScriptCreate : ITreeNodeAction
	{
		IConnectionManager _connectionManager;

		public ScriptCreate(IConnectionManager connectionManager)
		{
			_connectionManager = connectionManager;
		}

		public void Execute(SchemaInfo schemaInfo)
		{
			var f = false;
			var sb = new StringBuilder();
			sb.Append($"CREATE TABLE {schemaInfo.Schema}.\"{schemaInfo.NodeName}\"\r\n(");
			using (var connection = _connectionManager.GetConnection(schemaInfo.Database))
			{
				using (var command = connection.CreateCommand())
				{
					command.CommandText = $"SELECT column_name, data_type, character_maximum_length, is_nullable, column_default FROM information_schema.COLUMNS WHERE table_name = '{schemaInfo.NodeName}' and table_schema = '{schemaInfo.Schema}' order by ordinal_position;";
					using (var reader = command.ExecuteReader())
					{
						while (reader.Read())
						{
							if (f)
							{
								sb.Append(",\r\n");
							}
							sb.Append($"\t\"{reader.GetValue<string>("column_name")}\" {reader.GetValue<string>("data_type").ToUpper()}");
							var length = reader.GetValue<int>("character_maximum_length");
							if(length > 0)
							{
								sb.Append($" ({length})");
							}
							f = true;
							if(reader.GetValue<string>("is_nullable").ToUpper() == "NO")
							{
								sb.Append(" NOT NULL");
							}
							var def = reader.GetValue<string>("column_default");
							if(def != null)
							{
								sb.Append(" DEFAULT " + def.ToUpper());
							}
						}
					}
				}

				sb.Append("\r\n);\r\n\r\n");
				var hasIndexes = false;
				using (var command = connection.CreateCommand())
				{
					command.CommandText = $"SELECT pg_catalog.pg_get_userbyid(relowner) as owner, relhasindex FROM pg_catalog.pg_class c INNER JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace WHERE c.relkind IN('r','') AND n.nspname = '{schemaInfo.Schema}' AND c.relname='{schemaInfo.NodeName}'";
					using (var reader = command.ExecuteReader())
					{
						if(!reader.Read())
						{
							throw new System.Exception("Not found");
						}
						sb.Append($"ALTER TABLE {schemaInfo.Schema}.\"{schemaInfo.NodeName}\" OWNER TO {reader.GetValue<string>("owner")};\r\n");
						hasIndexes = reader.GetValue<bool>("relhasindex");
					}
				}

				if(hasIndexes)
				{
					using (var command = connection.CreateCommand())
					{
						command.CommandText = $"SELECT i.indexname, i.indexdef, c.conname FROM pg_indexes i LEFT JOIN pg_catalog.pg_constraint c on i.indexname = c.conname WHERE schemaname = '{schemaInfo.Schema}' and tablename='{schemaInfo.NodeName}' ORDER BY indexname desc;";
						using (var reader = command.ExecuteReader())
						{
							while(reader.Read())
							{
								var conname = reader.GetValue<string>("conname");
								if(conname == null)
								{
									sb.Append($"\r\n{reader.GetValue<string>("indexdef")}");
								}
							}
						}
					}
				}

			}
			var scriptEditor = new SqlQueryEngine(_connectionManager, schemaInfo.Database);
			var args = new ObjectViewerSqlEditorArgs() { Script = sb.ToString(), Element = scriptEditor };
			TriggerManager.Fire<ObjectViewerSqlEditor>(args);
		}
	}
}
