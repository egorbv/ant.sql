﻿using Ant.Sql.Common.Management;
using Ant.Sql.Common.Management.Interfaces;
using Ant.Sql.Common.Management.Triggers;
using Ant.Sql.Management.Postgres._10._5.ObjectViewer;
using static Ant.Sql.Management.Postgres._10._5.TreeNodeManagers.TreeNodeManagerBase;
using System.Text;

namespace Ant.Sql.Management.Postgres._10._5.TreeNodeManagers.Table
{
	public class ScriptDelete : ITreeNodeAction
	{
		IConnectionManager _connectionManager;

		public ScriptDelete(IConnectionManager connectionManager)
		{
			_connectionManager = connectionManager;
		}

		public void Execute(SchemaInfo schemaInfo)
		{
			var f = false;
			var sb = new StringBuilder();
			sb.Append($"DELETE FROM {schemaInfo.Schema}.\"{schemaInfo.NodeName}\"\r\n\tWHERE <condition>");
			var scriptEditor = new SqlQueryEngine(_connectionManager, schemaInfo.Database);
			var args = new ObjectViewerSqlEditorArgs() { Script = sb.ToString(), Element = scriptEditor };
			TriggerManager.Fire<ObjectViewerSqlEditor>(args);
		}
	}
}
