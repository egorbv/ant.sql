﻿
namespace Ant.Sql.Management.Postgres._10._5.TreeNodeManagers.Table
{
	public class NodeActions
	{
		public const int ScriptCreate = 100;
		public const int ScriptDelete = 101;
		public const int ScriptInsert = 102;
		public const int ScriptUpdate = 103;
		public const int ScriptSelect = 104;

		public const int TableDelete = 200;
	}
}
