﻿using Ant.Sql.Common.Management;
using Ant.Sql.Common.Management.Interfaces;
using Ant.Sql.Common.Management.Triggers;
using Ant.Sql.Management.Postgres._10._5.ObjectViewer;
using static Ant.Sql.Management.Postgres._10._5.TreeNodeManagers.TreeNodeManagerBase;
using System.Data.Common;
using System.Text;

namespace Ant.Sql.Management.Postgres._10._5.TreeNodeManagers.Table
{
	public class ScriptUpdate : ITreeNodeAction
	{
		IConnectionManager _connectionManager;

		public ScriptUpdate(IConnectionManager connectionManager)
		{
			_connectionManager = connectionManager;
		}

		public void Execute(SchemaInfo schemaInfo)
		{
			var f = false;
			var sb = new StringBuilder();
			sb.Append($"UPDATE {schemaInfo.Schema}.\"{schemaInfo.NodeName}\"\r\n\tSET\r\n ");
			using (var connection = _connectionManager.GetConnection(schemaInfo.Database))
			{
				using (var command = connection.CreateCommand())
				{
					command.CommandText = $"SELECT column_name FROM information_schema.COLUMNS WHERE table_name = '{schemaInfo.NodeName}' and table_schema = '{schemaInfo.Schema}' order by ordinal_position;";
					using (var reader = command.ExecuteReader())
					{
						while (reader.Read())
						{
							if (f)
							{
								sb.Append(",\r\n");
							}
							sb.Append($"\t\t\"{reader.GetValue<string>("column_name")}\" = ?");
							f = true;
						}
					}
				}
			}
			sb.Append("\r\n\tWHERE");
			sb.Append($"\r\n\t\t<condition>;");
			var scriptEditor = new SqlQueryEngine(_connectionManager, schemaInfo.Database);
			var args = new ObjectViewerSqlEditorArgs() { Script = sb.ToString(), Element = scriptEditor };
			TriggerManager.Fire<ObjectViewerSqlEditor>(args);
		}
	}
}
