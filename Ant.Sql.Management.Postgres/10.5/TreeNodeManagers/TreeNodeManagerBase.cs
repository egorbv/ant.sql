﻿using Ant.Sql.Common.Management.Interfaces;
using Ant.Sql.Common.Management.Models;
using System;
using System.Collections.Generic;

namespace Ant.Sql.Management.Postgres._10._5.TreeNodeManagers
{
	public abstract class TreeNodeManagerBase
	{
		protected Dictionary<int, ITreeNodeAction> actions;


		public virtual List<TreeNode> GetChildren(IConnectionManager connectionManager, TreeNodeContext context)
		{
			return null;
		}

		public TreeContextMenu GetContextMenu(IConnectionManager connectionManager, TreeNodeContext context)
		{
			if(actions == null)
			{
				createContextMenuHandlers(connectionManager, context);
			}
			return getContextMenu(connectionManager, context);
		}

		protected virtual TreeContextMenu getContextMenu(IConnectionManager connectionManager, TreeNodeContext context)
		{
			return null;
		}

		protected abstract void createContextMenuHandlers(IConnectionManager connectionManager, TreeNodeContext context);


		public void ExecuteTreeAction(IConnectionManager connectionManager, TreeNodeContext context, int actionID)
		{
			if (actions.ContainsKey(actionID))
			{
				var schemaInfo = context.GetSchemaInfo();
				actions[actionID].Execute(schemaInfo);
			}
		}


		public class SchemaInfo
		{
			public string Database;
			public string Schema;
			public string NodeName;
		}
	}

}
