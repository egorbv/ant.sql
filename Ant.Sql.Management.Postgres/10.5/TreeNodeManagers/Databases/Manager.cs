﻿using System.Data.Common;
using System.Collections.Generic;
using Ant.Sql.Common.Management.Interfaces;
using Ant.Sql.Common.Management.Models;

namespace Ant.Sql.Management.Postgres._10._5.TreeNodeManagers.Databases
{
	public class Manager : TreeNodeManagerBase
	{
		public override List<TreeNode> GetChildren(IConnectionManager connectionManager, TreeNodeContext context)
		{
			var result = new List<TreeNode>();
			using (var connection = connectionManager.GetConnection())
			{
				using (var command = connection.CreateCommand())
				{
					command.CommandText = "SELECT datname FROM pg_database WHERE datistemplate = false";
					using (var reader = command.ExecuteReader())
					{
						while (reader.Read())
						{
							var databaseName = reader.GetValue<string>("datname");
							result.Add(new TreeNode(TreeNodeType.Database, databaseName));
						}
					}
				}
			}
			return result;
		}

		protected override void createContextMenuHandlers(IConnectionManager connectionManager, TreeNodeContext context)
		{
		}
	}
}
