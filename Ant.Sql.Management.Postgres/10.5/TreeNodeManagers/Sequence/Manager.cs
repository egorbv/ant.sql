﻿using System.Collections.Generic;
using Ant.Sql.Common.Management.Interfaces;
using Ant.Sql.Common.Management.Models;

namespace Ant.Sql.Management.Postgres._10._5.TreeNodeManagers.Sequence
{
	public class Manager : TreeNodeManagerBase
	{
		protected override TreeContextMenu getContextMenu(IConnectionManager connectionManager, TreeNodeContext context)
		{
			var result = new TreeContextMenu();
			result.Items.Add(new TreeContextMenuItem() { Text = "Удалить", ActionID = NodeActions.SequenceDelete });
			return result;
		}

		protected override void createContextMenuHandlers(IConnectionManager connectionManager, TreeNodeContext context)
		{
			actions = new Dictionary<int, ITreeNodeAction>
			{
				[NodeActions.SequenceDelete] = new SequenceDelete(connectionManager),
			};
		}
	}
}
