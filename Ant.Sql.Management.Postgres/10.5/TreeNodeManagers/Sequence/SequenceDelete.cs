﻿using Ant.Sql.Common.Management.Interfaces;
using Ant.Sql.Common.WPF;
using System;
using static Ant.Sql.Management.Postgres._10._5.TreeNodeManagers.TreeNodeManagerBase;

namespace Ant.Sql.Management.Postgres._10._5.TreeNodeManagers.Sequence
{
	public class SequenceDelete : ITreeNodeAction
	{
		IConnectionManager _connectionManager;

		public SequenceDelete(IConnectionManager connectionManager)
		{
			_connectionManager = connectionManager;
		}

		public void Execute(SchemaInfo schemaInfo)
		{
			var dialog = new DropObject($"Вы уверены что хотите удалить последовательность \"{schemaInfo.Schema}.{schemaInfo.NodeName}\" ?");
			if (dialog.ShowDialog() == true)
			{
				try
				{
					using (var connection = _connectionManager.GetConnection(schemaInfo.Database))
					{
						using (var command = connection.CreateCommand())
						{
							command.CommandText = $"DROP SEQUENCE {schemaInfo.Schema}.\"{schemaInfo.NodeName}\"";
							command.ExecuteNonQuery();
						}
					}
				}
				catch (Exception e)
				{

				}
			}
		}
	}
}
