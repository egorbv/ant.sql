﻿using System.Data.Common;
using System.Collections.Generic;
using Ant.Sql.Common.Management.Interfaces;
using Ant.Sql.Common.Management.Models;

namespace Ant.Sql.Management.Postgres._10._5.TreeNodeManagers.Users
{
	public class Manager : TreeNodeManagerBase
	{
		public override List<TreeNode> GetChildren(IConnectionManager connectionManager, TreeNodeContext context)
		{
			var result = new List<TreeNode>();
			using (var connection = connectionManager.GetConnection())
			{
				using (var command = connection.CreateCommand())
				{
					command.CommandText = "SELECT u.usename FROM pg_catalog.pg_user u";
					using (var reader = command.ExecuteReader())
					{
						while (reader.Read())
						{
							result.Add(new TreeNode(TreeNodeType.User, reader.GetValue<string>("usename")));
						}
					}
				}
			}
			return result;
		}

		protected override void createContextMenuHandlers(IConnectionManager connectionManager, TreeNodeContext context)
		{
		}
	}
}
