﻿using System.Data.Common;
using System.Collections.Generic;
using Ant.Sql.Common.Management.Interfaces;
using Ant.Sql.Common.Management.Models;

namespace Ant.Sql.Management.Postgres._10._5.TreeNodeManagers.Schemas
{
	public class Manager : TreeNodeManagerBase
	{
		public override List<TreeNode> GetChildren(IConnectionManager connectionManager, TreeNodeContext context)
		{
			var schemaInfo = context.GetSchemaInfo();
			var result = new List<TreeNode>();
			using (var connection = connectionManager.GetConnection(schemaInfo.Database))
			{
				using (var command = connection.CreateCommand())
				{
					command.CommandText = string.Format("SELECT n.nspname FROM pg_catalog.pg_namespace n WHERE n.nspname !~'^pg_' AND n.nspname <> 'information_schema'", 1);
					using (var reader = command.ExecuteReader())
					{
						while (reader.Read())
						{
							var schemaName = reader.GetValue<string>("nspname");
							result.Add(new TreeNode(TreeNodeType.Schema, schemaName));
						}
					}
				}
			}
			return result;
		}

		protected override void createContextMenuHandlers(IConnectionManager connectionManager, TreeNodeContext context)
		{
		}
	}
}
