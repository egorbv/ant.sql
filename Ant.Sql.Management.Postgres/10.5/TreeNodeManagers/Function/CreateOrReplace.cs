﻿using Ant.Sql.Common;
using Ant.Sql.Common.Management;
using Ant.Sql.Common.Management.Interfaces;
using Ant.Sql.Common.Management.Triggers;
using Ant.Sql.Management.Postgres._10._5.ObjectViewer;
using System;
using System.Data.Common;
using static Ant.Sql.Management.Postgres._10._5.TreeNodeManagers.TreeNodeManagerBase;

namespace Ant.Sql.Management.Postgres._10._5.TreeNodeManagers.Function
{
	public class CreateOrReplace : ITreeNodeAction
	{
		IConnectionManager _connectionManager;

		public CreateOrReplace(IConnectionManager connectionManager)
		{
			_connectionManager = connectionManager;
		}

		public void Execute(SchemaInfo schemaInfo)
		{
			using (var connection = _connectionManager.GetConnection(schemaInfo.Database))
			{
				using (var command = connection.CreateCommand())
				{
					command.CommandText = $"select pg_get_functiondef(p.oid) as def from  pg_proc p join pg_namespace n on n.oid=pronamespace and n.nspname='{schemaInfo.Schema}' where p.proname='{schemaInfo.NodeName}'";
					using (var reader = command.ExecuteReader())
					{
						if (!reader.Read())
						{
							throw new System.Exception("Function not found");
						}
						var text = reader.GetValue<string>("def");
						var scriptEditor = new SqlQueryEngine(_connectionManager, schemaInfo.Database);
						var args = new ObjectViewerSqlEditorArgs() { Script = text, Element = scriptEditor };
						TriggerManager.Fire<ObjectViewerSqlEditor>(args);
					}
				}
			}
		}

	}
}
