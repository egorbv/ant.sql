﻿using System.Collections.Generic;
using Ant.Sql.Common.Management.Interfaces;
using Ant.Sql.Common.Management.Models;

namespace Ant.Sql.Management.Postgres._10._5.TreeNodeManagers.Function
{
	public class Manager : TreeNodeManagerBase
	{

		protected override TreeContextMenu getContextMenu(IConnectionManager connectionManager, TreeNodeContext context)
		{
			var result = new TreeContextMenu();
			var script = new TreeContextMenuItem() { Text = "Скрипт", Items = new List<TreeContextMenuItem>() };
			script.Items.Add(new TreeContextMenuItem() { Text = "Создать или заместить", ActionID = NodeActions.ScriptCreateOrReplace });
			result.Items.Add(script);
			return result;
		}

		protected override void createContextMenuHandlers(IConnectionManager connectionManager, TreeNodeContext context)
		{
			actions = new Dictionary<int, ITreeNodeAction>
			{
				[NodeActions.ScriptCreateOrReplace] = new CreateOrReplace(connectionManager)
			};
		}
	}
}
