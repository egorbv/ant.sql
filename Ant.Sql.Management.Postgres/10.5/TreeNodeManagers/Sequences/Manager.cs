﻿using System.Data.Common;
using System.Collections.Generic;
using Ant.Sql.Common.Management.Interfaces;
using Ant.Sql.Common.Management.Models;

namespace Ant.Sql.Management.Postgres._10._5.TreeNodeManagers.Sequences
{
	public class Manager : TreeNodeManagerBase
	{
		public override List<TreeNode> GetChildren(IConnectionManager connectionManager, TreeNodeContext context)
		{
			var schemaInfo = context.GetSchemaInfo();
			var result = new List<TreeNode>();
			using (var connection = connectionManager.GetConnection(schemaInfo.Database))
			{
				using (var command = connection.CreateCommand())
				{
					command.CommandText = $"SELECT s.relname FROM pg_class s JOIN pg_namespace n ON n.oid = s.relnamespace WHERE s.relkind='S' AND n.nspname= '{schemaInfo.Schema}'";
					using (var reader = command.ExecuteReader())
					{
						while (reader.Read())
						{
							result.Add(new TreeNode(TreeNodeType.Sequence, reader.GetValue<string>("relname")));
						}
					}
				}
			}
			return result;
		}

		protected override void createContextMenuHandlers(IConnectionManager connectionManager, TreeNodeContext context)
		{
		}

	}
}
