﻿namespace Ant.Sql.Management.Postgres._10._5.TreeNodeManagers
{
	public class TreeActionType
	{
		public const int SystemDelete =1;
		public const int SystemUpdate = 2;
		public const int SystemRefresh = 3;
		public const int SystemDisconnect = 4;

	}
}
