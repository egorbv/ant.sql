﻿using System;
using System.Data.Common;
using System.Collections.Generic;
using Ant.Sql.Common.Management.Interfaces;
using Ant.Sql.Common.Management.Models;

namespace Ant.Sql.Management.Postgres._10._5.TreeNodeManagers.Tables
{
	public class Manager : TreeNodeManagerBase
	{
		Guid SizeOfTablesReportCommand = new Guid("09F21F6D-98B1-44E6-97A0-859730639C8A");
		
		public override List<TreeNode> GetChildren(IConnectionManager connectionManager, TreeNodeContext context)
		{
			var schemaInfo = context.GetSchemaInfo();
			var result = new List<TreeNode>();
			using (var connection = connectionManager.GetConnection(schemaInfo.Database))
			{
				using (var command = connection.CreateCommand())
				{
					command.CommandText = string.Format("select * from pg_tables where schemaname='{0}'", schemaInfo.Schema);
					using (var reader = command.ExecuteReader())
					{
						while (reader.Read())
						{
							var tableName = reader.GetValue<string>("tablename");
							result.Add(new TreeNode(TreeNodeType.Table, tableName));
						}
					}
				}
			}
			return result;
		}

		protected override TreeContextMenu getContextMenu(IConnectionManager connectionManager, TreeNodeContext context)
		{
			var result = new TreeContextMenu();
			var scripts = new TreeContextMenuItem() { Text = "Отчеты", Items= new List<TreeContextMenuItem>() };
			scripts.Items.Add(new TreeContextMenuItem() { Text = "Размер таблиц", ActionID = NodeActions.ReportSizeOfTables });
			result.Items.Add(scripts);
			return result;
		}

		protected override void createContextMenuHandlers(IConnectionManager connectionManager, TreeNodeContext context)
		{
			actions = new Dictionary<int, ITreeNodeAction>
			{
				[NodeActions.ReportSizeOfTables] = new ReportSizeOfTables(connectionManager),
			};
		}
	}
}



//SELECT nspname || '.' || relname AS "relation",
//    pg_size_pretty(pg_total_relation_size(C.oid)) AS "total_size", c.*
//  FROM pg_class C
//  LEFT JOIN pg_namespace N ON(N.oid = C.relnamespace)
//  WHERE nspname NOT IN('pg_catalog', 'information_schema')

//	AND C.relkind<> 'i'
//    AND nspname !~ '^pg_toast'
//  ORDER BY pg_total_relation_size(C.oid) DESC
//  LIMIT 50;
