﻿using Ant.Sql.Common.Management;
using Ant.Sql.Common.Management.Interfaces;
using Ant.Sql.Common.Management.Triggers;
using Ant.Sql.Management.Postgres._10._5.ObjectViewer;
using static Ant.Sql.Management.Postgres._10._5.TreeNodeManagers.TreeNodeManagerBase;

namespace Ant.Sql.Management.Postgres._10._5.TreeNodeManagers.Tables
{
	public class ReportSizeOfTables : ITreeNodeAction
	{
		IConnectionManager _connectionManager;

		public ReportSizeOfTables(IConnectionManager connectionManager)
		{
			_connectionManager = connectionManager;
		}

		public void Execute(SchemaInfo schemaInfo)
		{
			var script = $"SELECT relname as \"TableName\", pg_size_pretty(pg_total_relation_size(C.oid)) AS \"PettySize\" , pg_total_relation_size(C.oid) as \"RawSize\" FROM pg_class c LEFT JOIN pg_namespace N ON(N.oid = C.relnamespace) WHERE nspname = '{schemaInfo.Schema}' AND C.relkind = 'r' ORDER BY relname";
			var report = new Report(_connectionManager, schemaInfo, script);
			var header = $"Отчет:Размер таблиц - {schemaInfo.Database}.{schemaInfo.Schema}";
			TriggerManager.Fire<ObjectViewerElement>(new ObjectViewerElementArgs() { Element = report, Header = header });
		}
	}
}
