﻿using static Ant.Sql.Management.Postgres._10._5.TreeNodeManagers.TreeNodeManagerBase;

namespace Ant.Sql.Common.Management.Models
{
	public static class TreeNodeContextExtension
	{
		public static SchemaInfo GetSchemaInfo(this TreeNodeContext  context)
		{
			var result = new SchemaInfo() { NodeName = context.CurrentNode.Name };
			if(context.CurrentNode.Type == TreeNodeType.Database)
			{
				result.Database = context.CurrentNode.Name;
			}
			foreach (var node in context.Parents)
			{
				switch (node.Type)
				{
					case TreeNodeType.Database:
						result.Database = node.Name;
						break;
					case TreeNodeType.Schema:
						result.Schema = node.Name;
						break;
				}
			}
			return result;
		}
	}
}
