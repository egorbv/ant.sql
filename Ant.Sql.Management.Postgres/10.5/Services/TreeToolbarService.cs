﻿using Ant.Sql.Common.Management;
using Ant.Sql.Common.Management.Interfaces;
using Ant.Sql.Common.Management.Models;
using Ant.Sql.Common.Management.Triggers;
using Ant.Sql.Management.Postgres._10._5.TreeNodeManagers;
using System;
using System.Collections.Generic;

namespace Ant.Sql.Management.Postgres._10._5.Services
{
	public class TreeToolbarService : ITreeToolbarService
	{
		protected ITreeChildrenService treeChildrenService;
		protected List<ToolbarItem> toolbarItems;


		public TreeToolbarService(ITreeChildrenService treeChildrenService)
		{
			this.treeChildrenService = treeChildrenService;
			toolbarItems = new List<ToolbarItem>
			{
				new ToolbarItem() { Text = "Отседенить", ActionID = TreeActionType.SystemDisconnect, Disabed = false },
				new ToolbarItem() { Text = "Обновить", ActionID = TreeActionType.SystemRefresh }
			};
		}

		public void Execute(TreeNodeContext context, int actionID)
		{
			if (actionID == TreeActionType.SystemRefresh)
			{
				var args = new TreeSystemCommandArgs() { CommandType = TreeCommandType.Refresh, Node = context.CurrentNode };
				TriggerManager.Fire<TreeSystemCommand>(args);
				return;
			}
			if(actionID == TreeActionType.SystemDisconnect)
			{
				var args = new TreeSystemCommandArgs() { CommandType = TreeCommandType.Disconnect, Node = context.CurrentNode };
				TriggerManager.Fire<TreeSystemCommand>(args);
				return;
			}
			treeChildrenService.ExecuteTreeAction(context, actionID);
		}

		public List<ToolbarItem> GetItems(TreeNode node)
		{
			toolbarItems[1].Disabed = false;
			switch(node.Type)
			{
				case TreeNodeType.Database:
					toolbarItems[1].Disabed = true;
					break;
			}
			return toolbarItems;
		}


	}
}
