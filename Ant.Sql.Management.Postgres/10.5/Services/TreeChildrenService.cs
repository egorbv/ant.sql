﻿using Ant.Sql.Common.Management.Interfaces;
using Ant.Sql.Common.Management.Models;
using Ant.Sql.Management.Postgres._10._5.TreeNodeManagers;
using System.Collections.Generic;

namespace Ant.Sql.Management.Postgres._10._5.Services
{
	public class TreeChildrenService : ITreeChildrenService
	{
		protected IConnectionManager connectionManager;
		protected Dictionary<TreeNodeType, TreeNodeManagerBase> nodeManagers;

		public TreeChildrenService(IConnectionManager connectionManager)
		{
			this.connectionManager = connectionManager;
			nodeManagers = new Dictionary<TreeNodeType, TreeNodeManagerBase>
			{
				[TreeNodeType.ConnectorOffline] = new TreeNodeManagers.Root.Manager(),
				[TreeNodeType.Databases] = new TreeNodeManagers.Databases.Manager(),
				[TreeNodeType.Database] = new TreeNodeManagers.Database.Manager(),
				[TreeNodeType.Schemas] = new TreeNodeManagers.Schemas.Manager(),
				[TreeNodeType.Schema] = new TreeNodeManagers.Schema.Manager(),
				[TreeNodeType.Functions] = new TreeNodeManagers.Functions.Manager(),
				[TreeNodeType.Tables] = new TreeNodeManagers.Tables.Manager(),
				[TreeNodeType.Table] = new TreeNodeManagers.Table.Manager(),
				[TreeNodeType.Users] = new TreeNodeManagers.Users.Manager(),
				[TreeNodeType.Sequence] = new TreeNodeManagers.Sequence.Manager(),
				[TreeNodeType.Sequences] = new TreeNodeManagers.Sequences.Manager(),
				[TreeNodeType.Securities] = new TreeNodeManagers.Securities.Manager(),
				[TreeNodeType.Function] = new TreeNodeManagers.Function.Manager(),
			};
		}

		public void ExecuteTreeAction(TreeNodeContext context, int actionID)
		{
			if (nodeManagers.ContainsKey(context.CurrentNode.Type))
			{
				nodeManagers[context.CurrentNode.Type].ExecuteTreeAction(connectionManager, context, actionID);
			}
		}

		public List<TreeNode> Get(TreeNodeContext context)
		{
			if(nodeManagers.ContainsKey(context.CurrentNode.Type))
			{
				return nodeManagers[context.CurrentNode.Type].GetChildren(connectionManager, context);
			}
			return null;
		}

		public TreeContextMenu GetContextMenu(TreeNodeContext context)
		{
			if (nodeManagers.ContainsKey(context.CurrentNode.Type))
			{
				return nodeManagers[context.CurrentNode.Type].GetContextMenu(connectionManager, context);
			}
			return null;
		}
	}


}
