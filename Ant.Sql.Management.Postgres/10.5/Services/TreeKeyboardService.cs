﻿using Ant.Sql.Common.Management.Interfaces;
using Ant.Sql.Common.Management.Models;
using Ant.Sql.Management.Postgres._10._5.TreeNodeManagers;

namespace Ant.Sql.Management.Postgres._10._5.Services
{
	public class TreeKeyboardService : ITreeKeyboardService
	{
		protected TreeChildrenService treeChildrenService;

		public TreeKeyboardService (TreeChildrenService treeChildrenService)
		{
			this.treeChildrenService = treeChildrenService;
		}

		public void Execute(TreeNodeContext context, KeyDownInfo keyDownInfo)
		{
			switch(keyDownInfo)
			{
				case KeyDownInfo.Ctrl_R:
				case KeyDownInfo.F5:
					treeChildrenService.ExecuteTreeAction(context, TreeActionType.SystemRefresh);
					break;
				case KeyDownInfo.Delete:
					treeChildrenService.ExecuteTreeAction(context, TreeActionType.SystemDelete);
					break;
				case KeyDownInfo.Update:
					treeChildrenService.ExecuteTreeAction(context, TreeActionType.SystemUpdate);
					break;
			}
		}
	}
}
