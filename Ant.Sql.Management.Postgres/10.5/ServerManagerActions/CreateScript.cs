﻿using Ant.Sql.Common;
using Ant.Sql.Common.Management;
using Ant.Sql.Common.Management.Interfaces;
using Ant.Sql.Common.Management.Triggers;
using Ant.Sql.Management.Postgres._10._5.ObjectViewer;
using static Ant.Sql.Management.Postgres._10._5.TreeNodeManagers.TreeNodeManagerBase;

namespace Ant.Sql.Management.Postgres._10._5.ServerManagerActions
{
	public interface IServerManagerAction
	{
		void Execute(SchemaInfo schemaInfo);
	}

	public class CreateScript : IServerManagerAction
	{
		readonly IConnectionManager _connectionManager;

		public CreateScript(IConnectionManager connectionManager)
		{
			_connectionManager = connectionManager;
		}

		public void Execute(SchemaInfo schemaInfo)
		{
			var scriptEditor = new SqlQueryEngine(_connectionManager, schemaInfo.Database);
			var args = new ObjectViewerSqlEditorArgs() { Script = "", Element = scriptEditor };
			TriggerManager.Fire<ObjectViewerSqlEditor>(args);
		}
	}
}
